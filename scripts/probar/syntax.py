import os
import re
import subprocess

import common
import test


unescapedSlash = re.compile("error: line (\d+): 0 is wrong flag id")
multipleDefinitions = re.compile(
    "error: line (\d+): multiple definitions of an affix flag"
)


def check(path):
    errors = 0

    args = [
        "hunspell",
        "-d",
        path,
        "-l",
    ]
    hunspell = subprocess.Popen(
        args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    standardOutput, errorOutput = hunspell.communicate("proba".encode())
    for line in errorOutput.splitlines():
        line = line.decode()
        errors += 1

        match = unescapedSlash.match(line)
        if match:
            lineNumber = int(match.group(1))
            lineContent = common.getLineFromFile(path + ".dic", lineNumber)
            common.error(
                f"Hai unha barra inclinada («/») sen escapar na liña "
                f"{lineNumber} do dicionario («{lineContent}»)."
            )
            continue

        match = multipleDefinitions.match(line)
        if match:
            lineNumber = int(match.group(1))
            lineContent = common.getLineFromFile(path + ".dic", lineNumber)
            common.error(
                "Definición duplicada de marca na liña {} do dicionario («{}»).".format(
                    lineNumber, lineContent
                )
            )
            continue

        common.error(
            "O executábel de Hunspell informou do seguinte erro: «{}».".format(line)
        )

    return errors


class SyntaxTest(test.Test):
    def __init__(self):
        super(SyntaxTest, self).__init__()

        self.path = "sintaxe"

    def getAllModules(self):
        modules = []
        rootPath, folderNames, fileNames = next(os.walk(common.getSourcePath()))
        for folderName in folderNames:
            modules.append(folderName)
        for fileName in fileNames:
            modules.append(fileName)
        return modules

    def run(self, spellCheckerManager):
        modules = self.getAllModules()
        config = {
            "aff": modules,
            "dic": modules,
            "rep": modules,
        }
        self.spellCheckerPath = spellCheckerManager.create(config)
        self.errors += check(self.spellCheckerPath)
        self.report()


def loadTestList():
    return [
        SyntaxTest(),
    ]
