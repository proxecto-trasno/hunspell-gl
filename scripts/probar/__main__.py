import sys
from sys import exit

import common
import spellchecker
import spelling
import syntax


def loadBuiltInTestList():
    builtInTests = []
    builtInTests.extend(spelling.loadTestList())
    builtInTests.extend(syntax.loadTestList())
    return builtInTests


# Program starts.

testsPath = common.getTestsPath()
builtInTests = loadBuiltInTestList()

found_errors = False
with spellchecker.Manager() as spellCheckerManager:
    for parameter in [parameter for parameter in sys.argv[1:]]:
        testPath = common.getFirstPathRelativeToSecondPathIfWithin(parameter, testsPath)
        executedTests = 0
        for test in builtInTests:
            if test.path.startswith(testPath):
                print(":: Executando «{path}»…".format(path=test.path))
                test.run(spellCheckerManager)
                executedTests += 1
                found_errors |= bool(test.errors)
        if executedTests == 0:
            print(":: Non existe ningunha proba para «{path}».".format(path=testPath))
if found_errors:
    exit(1)
