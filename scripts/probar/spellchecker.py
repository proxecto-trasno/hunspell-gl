import os
import subprocess

import common


class Manager(object):
    def __init__(self):
        self.builtSpellCheckers = {}

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        for path in list(self.builtSpellCheckers.keys()):
            os.remove(path + ".aff")
            os.remove(path + ".dic")

        try:
            os.rmdir(common.getBuildPath())
        except (FileNotFoundError, OSError):
            pass

    def getUnusedCode(self):
        buildPath = common.getBuildPath()

        if not os.path.exists(buildPath):
            return 1

        index = 2
        while os.path.exists(os.path.join(buildPath, "{}.aff".format(index))):
            index += 1
        return index

    def create(self, config):
        for builtPath, builtConfig in list(self.builtSpellCheckers.items()):
            if config == builtConfig:
                return builtPath

        args = [
            "scons",
        ]

        for fileExtension in ["aff", "dic", "rep"]:
            if fileExtension in config:
                args.append(
                    "{}={}".format(fileExtension, ",".join(config[fileExtension]))
                )

        code = self.getUnusedCode()
        args.append("code={}".format(code))

        open(os.devnull, "w")
        print(f"Subprocess call: {args}")
        print(f"Root: {common.getRootPath()}")
        subprocess.call(args, cwd=common.getRootPath(), stderr=subprocess.STDOUT)

        path = os.path.join(common.getBuildPath(), "{}".format(code))
        print(f"Built {path}")
        self.builtSpellCheckers[path] = config
        return path
