# -*- coding:utf-8 -*-

import textwrap

import codecs

from common import formatEntriesAndCommentsForDictionary, ContentCache, PdfParser
import generator



contentCache = ContentCache("usc")
pdfUrl = "http://www.usc.es/export/sites/default/gl/servizos/snl/asesoramento/fundamentos/descargas/abreviaturassiglassimboloslexico.pdf"


class AbbreviationsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "usc/abreviaturas.dic"


    def parseEntry(self, entry):
        if "./" in entry:
            for subentry in entry.split("/"):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif "," in entry:
            for subentry in entry.split(","):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif entry:
            yield entry


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(pdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        lineIsContinuation = False
        comment = None

        for line in pdfParser.lines():

            if comment and line[0] != " " and parsingStage == 1:
                lineIsContinuation = True
            line = line.strip()

            if parsingStage == 0:
                if line == "abril":
                    parsingStage += 1
                else:
                    continue

            elif parsingStage == 1:
                if line == "Manuel Bermúdez":
                    break

            if line.startswith("Abreviaturas, siglas, símbolos e léxico"):
                continue
            if line.isdigit():
                continue

            if lineIsContinuation:
                lineIsContinuation = False
                comment += " " + line
                continue

            if comment:
                for subentry in self.parseEntry(line):
                    entries[subentry] = comment
                comment = None
            else:
                comment = line


        dictionary  = "# Relación de abreviaturas máis frecuentes\n"
        dictionary += "# {}\n".format(pdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "abreviatura"):
            dictionary += entry
        return dictionary


class AcronymsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "usc/siglas.dic"


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(pdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        lineIsContinuation = False
        comment = None

        for line in pdfParser.lines():

            if comment and line[0] != " " and parsingStage == 1:
                lineIsContinuation = True
            line = line.strip()

            if parsingStage == 0:
                if line == "Asociación Española de Normalización e Certificación":
                    parsingStage += 1
                else:
                    continue

            elif parsingStage == 1:
                if line == "A sigla caracterízase por:":
                    break

            if line.isdigit():
                continue

            if lineIsContinuation:
                lineIsContinuation = False
                comment += " " + line
                continue

            if comment:
                entries[line] = comment
                comment = None
            else:
                comment = line


        dictionary  = "# Relación de siglas e acrónimos máis frecuentes\n"
        dictionary += "# {}\n".format(pdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "sigla"):
            dictionary += entry
        return dictionary


class SymbolsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "usc/símbolos.dic"


    def parseEntry(self, entry):
        if "./" in entry:
            for subentry in entry.split("/"):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif "," in entry:
            for subentry in entry.split(","):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif entry:
            yield entry


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(pdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        lineIsContinuation = False
        comment = None

        for line in pdfParser.lines():

            if comment and line[0] != " " and parsingStage == 1:
                lineIsContinuation = True
            line = line.strip()

            if parsingStage == 0:
                if line == "amperio":
                    parsingStage += 1
                else:
                    continue

            elif parsingStage == 1:
                if line == "Manuel Bermúdez":
                    break

            if line.isdigit():
                continue

            if lineIsContinuation:
                lineIsContinuation = False
                comment += " " + line
                continue

            if comment:
                if not comment.endswith("-"): # Saltarse os prefixos.
                    for subentry in self.parseEntry(line):
                        entries[subentry] = comment
                comment = None
            else:
                comment = line


        dictionary  = "# Relación de símbolos máis frecuentes\n"
        dictionary += "# {}\n".format(pdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "símbolo"):
            dictionary += entry
        return dictionary


def loadGeneratorList():
    generators = []
    generators.append(AbbreviationsGenerator())
    generators.append(AcronymsGenerator())
    generators.append(SymbolsGenerator())
    return generators