# -*- coding:utf-8 -*-

import textwrap

import codecs

from common import formatEntriesAndCommentsForDictionary, ContentCache, PdfParser
import generator



uvigoContentCache = ContentCache("uvigo")
doubtsPdfUrl = "http://anl.uvigo.es/UserFiles/File/manuais/Lingua_galega._Dubidas_linguisticas.pdf"


class AbbreviationsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "uvigo/abreviaturas.dic"


    def parseSubEntries(self, entry):
        if "," in entry:
            for subentry in entry.split(","):
                yield subentry
        elif "/ /" in entry:
            for subentry in entry.split("/ /"):
                yield subentry
        elif "/" in entry:
            for subentry in entry.split("/"):
                yield subentry
        else:
            yield entry


    def generateFileContent(self):

        filePath = uvigoContentCache.downloadFileIfNeededAndGetLocalPath(doubtsPdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0

        for line in pdfParser.lines():

            if parsingStage == 0:
                if line == "Relación de abreviaturas máis frecuentes":
                    parsingStage += 1
                else:
                    continue

            elif parsingStage == 1:
                if line == "Relación de siglas e acrónimos máis frecuentes":
                    parsingStage += 1
                    continue

            elif parsingStage == 2:
                if line == "49":
                    parsingStage += 1
                else:
                    continue

            elif parsingStage == 3:
                if line == "5":
                    break

            if ":" in line:
                comment, entry = line.split(":")
                for subentry in self.parseSubEntries(entry):
                    subentry = subentry.strip()
                    entries[subentry] = comment.strip()

        dictionary  = "# Relación de abreviaturas máis frecuentes\n"
        dictionary += "# {}\n".format(doubtsPdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "abreviatura"):
            dictionary += entry
        return dictionary


class AcronymsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "uvigo/siglas.dic"


    def generateFileContent(self):

        filePath = uvigoContentCache.downloadFileIfNeededAndGetLocalPath(doubtsPdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        commentCache = None

        for line in pdfParser.lines():

            if parsingStage == 0:
                if line == "Relación de siglas e acrónimos máis frecuentes":
                    parsingStage += 1
                else:
                    continue

            elif parsingStage == 1:
                if line == "49":
                    parsingStage += 1
                    continue

            elif parsingStage == 2:
                if line == "5":
                    parsingStage += 1
                else:
                    continue

            elif parsingStage == 3:
                if line == "55":
                    break

            if commentCache:
                entry = line.strip()
                entries[entry] = commentCache
                commentCache = None
            elif ":" in line:
                comment, entry = line.split(":")
                entry = entry.strip()
                if entry:
                    entries[entry] = comment.strip()
                else:
                    commentCache = comment.strip()

        dictionary  = "# Relación de acrónimos e siglas máis frecuentes\n"
        dictionary += "# {}\n".format(doubtsPdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "sigla"):
            dictionary += entry
        return dictionary


def loadGeneratorList():
    generators = []
    generators.append(AbbreviationsGenerator())
    generators.append(AcronymsGenerator())
    return generators