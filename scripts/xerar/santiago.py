# -*- coding:utf-8 -*-

import textwrap

import codecs

from common import formatEntriesAndCommentsForDictionary, ContentCache, PdfParser
import generator



contentCache = ContentCache("santiago")
styleGuidePdfUrl = "http://www.v1deputacionlugo.org/media/documentos/Libro_de_estilo_Concello_Santiago.pdf"


class AbbreviationsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "santiago/abreviaturas.dic"


    def parseEntry(self, entry):
        if "./" in entry:
            for subentry in entry.split("/"):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif "," in entry:
            for subentry in entry.split(","):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif ";" in entry:
            for subentry in entry.split(";"):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif entry:
            yield entry


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(styleGuidePdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        continuesInTheNextLine = False
        previousLine = None

        import re, string

        plural = re.compile("\(plural ([^)]+)\)")
        fem = re.compile("\(fem. ([^)]+)\)")
        parenthesis = re.compile(" *\([^)]*\)")

        for line in pdfParser.lines():

            if line[-1:] == " " and parsingStage == 1:
                continuesInTheNextLine = True
            line = line.strip()

            if parsingStage == 0:
                if line == "7.1.2 Listaxe de abreviaturas":
                    parsingStage += 1
                continue

            elif parsingStage == 1:
                if line == "7.2 A sigla":
                    break

            if line in string.uppercase:
                continue
            if line.startswith("Ortografía e estilo"):
                continue
            if line.isdigit():
                continue

            if continuesInTheNextLine:
                continuesInTheNextLine = False
                previousLine = line
                continue

            if previousLine:
                line = previousLine + " " + line
                previousLine = None

            try:
                comment, entry = line.split(":")
            except ValueError:
                parts = line.split(":")
                comment = ":".join(parts[:-1])
                entry = parts[-1]

            subentries = set()

            for match in plural.finditer(entry):
                for subentry in self.parseEntry(match.group(1)):
                    subentries.add(subentry)

            for match in fem.finditer(entry):
                for subentry in self.parseEntry(match.group(1)):
                    subentries.add(subentry)

            entry = re.sub(parenthesis, "", entry) # Eliminar contido entre parénteses.
            entry = entry.strip()

            for subentry in self.parseEntry(entry):
                subentries.add(subentry)

            for subentry in subentries:
                entries[subentry] = comment

        dictionary  = "# Relación de abreviaturas máis frecuentes na linguaxe administrativa\n"
        dictionary += "# {}\n".format(styleGuidePdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "abreviatura"):
            dictionary += entry
        return dictionary


class AcronymsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "santiago/siglas.dic"


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(styleGuidePdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        continuesInTheNextLine = False
        previousLine = None

        import string

        for line in pdfParser.lines():

            if line[-1:] in [" ", "-"] and parsingStage == 1:
                continuesInTheNextLine = True

            if parsingStage == 0:
                if line == "7.2.2 Listaxe de siglas e acrónimos de uso común":
                    parsingStage += 1
                continue

            elif parsingStage == 1:
                if line == "7.3 O símbolo":
                    break

            if line in string.uppercase:
                continue
            if line.startswith("Ortografía e estilo"):
                continue
            if line.isdigit():
                continue

            if previousLine:
                line = previousLine + line
                previousLine = None

            if continuesInTheNextLine:
                continuesInTheNextLine = False
                previousLine = line
                continue

            if ":" not in line:
                parts = line.split(" ")
                entry = parts[0]
                comment = " ".join(parts[1:])
            else:
                try:
                    entry, comment = line.split(":")
                except ValueError:
                    parts = line.split(":")
                    entry = parts[0]
                    comment = ":".join(parts[1:])

            entries[entry.strip()] = comment

        dictionary  = "# Relación de siglas máis frecuentes\n"
        dictionary += "# {}\n".format(styleGuidePdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "sigla"):
            dictionary += entry
        return dictionary


class SymbolsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "santiago/símbolos.dic"


    def parseEntry(self, entry):
        entry = ''.join(dict(list(zip("0123456789", "⁰¹²³⁴⁵⁶⁷⁸⁹"))).get(c, c) for c in entry) # http://stackoverflow.com/a/13875688/939364
        if " ou " in entry:
            for subentry in entry.split(" ou "):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif entry in ["MXP/MXN", "PES/PEN", "ROL/RON", "TRL/TRY"]:
            for subentry in entry.split("/"):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif entry.endswith("-") or entry.startswith("-"):
            pass # Prefixo ou sufixo.
        elif entry:
            yield entry


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(styleGuidePdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        continuesInTheNextLine = False
        previousLine = None

        for line in pdfParser.lines():

            if line[-1:] in [" ", "-"] and parsingStage == 1:
                continuesInTheNextLine = True

            if parsingStage == 0:
                if line == "7.3.2 Listaxe de símbolos de uso común":
                    parsingStage += 1
                continue

            elif parsingStage == 1:
                if line == "7.4 O acrónimo":
                    break

            if line.startswith("Ortografía e estilo"):
                continue
            if line.isdigit():
                continue

            if previousLine:
                line = previousLine + line
                previousLine = None

            if continuesInTheNextLine:
                continuesInTheNextLine = False
                previousLine = line
                continue

            if ":" not in line:
                parts = line.split(" ")
                entry = parts[0]
                comment = " ".join(parts[1:])
            else:
                try:
                    entry, comment = line.split(":")
                except ValueError:
                    parts = line.split(":")
                    entry = parts[0]
                    comment = ":".join(parts[1:])

            entry = entry.strip()
            for subentry in self.parseEntry(entry):
                entries[subentry] = comment

        dictionary  = "# Relación de símbolos máis frecuentes\n"
        dictionary += "# {}\n".format(styleGuidePdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "símbolo"):
            dictionary += entry
        return dictionary


def loadGeneratorList():
    generators = []
    generators.append(AcronymsGenerator())
    generators.append(AbbreviationsGenerator())
    generators.append(SymbolsGenerator())
    return generators