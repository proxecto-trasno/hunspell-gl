# -*- coding:utf-8 -*-

import textwrap

import codecs

from common import formatEntriesAndCommentsForDictionary, ContentCache, PdfParser
import generator



contentCache = ContentCache("ceg")
pdfUrl = "http://www.normalizacion.ceg.es/attachments/article/71/siglaspdf.pdf"


class AbbreviationsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "ceg/abreviaturas.dic"


    def parseEntry(self, entry):
        if "./" in entry:
            for subentry in entry.split("/"):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif "," in entry:
            for subentry in entry.split(","):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif ";" in entry:
            for subentry in entry.split(";"):
                subentry = subentry.strip()
                if subentry:
                    yield subentry
        elif entry:
            yield entry


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(pdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        entry = None

        import re

        plural = re.compile("\(pl. ([^)]+)\)")
        parenthesis = re.compile(" *\([^)]*\)")

        for line in pdfParser.lines():

            line = line.strip()
            if not line:
                continue
            if line.isdigit():
                continue

            if parsingStage == 0:
                if line == "ABREVIATURAS:":
                    parsingStage += 1
                continue

            elif parsingStage == 1:
                if line == "SÍMBOLOS:":
                    break

            parts = line.split(":")
            comment = parts[0].strip()
            entry = ":".join(parts[1:]).strip()

            subentries = set()

            for match in plural.finditer(entry):
                for subentry in self.parseEntry(match.group(1)):
                    subentries.add(subentry)

            entry = re.sub(parenthesis, "", entry) # Eliminar contido entre parénteses.
            entry = entry.strip()

            for subentry in self.parseEntry(entry):
                if subentry.endswith("o/a."):
                    subentries.add(subentry[:-4] + "a.")
                    subentries.add(subentry[:-4] + "o.")
                else:
                    subentries.add(subentry)

            for subentry in subentries:
                entries[subentry] = comment

        dictionary  = "# Relación de abreviaturas máis frecuentes\n"
        dictionary += "# {}\n".format(pdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "abreviatura"):
            dictionary += entry
        return dictionary


class AcronymsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "ceg/siglas.dic"


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(pdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0
        entry = None

        for line in pdfParser.lines():

            line = line.strip()
            if not line:
                continue

            if parsingStage == 0:
                if line == "SIGLAS e ACRÓNIMOS":
                    parsingStage += 1
                continue

            elif parsingStage == 1:
                if line == "ABREVIATURAS:":
                    break

            if line.isdigit():
                continue

            parts = line.split(":")
            if parts[0].upper() != parts[0]:
                comment += " " + parts[0].strip()
                entries[entry] = comment
            else:
                entry = parts[0].strip()
                comment = ":".join(parts[1:]).strip()
                entries[entry] = comment

        dictionary  = "# Relación de siglas e acrónimos máis frecuentes\n"
        dictionary += "# {}\n".format(pdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "sigla"):
            dictionary += entry
        return dictionary


class SymbolsGenerator(generator.Generator):

    def __init__(self):
        self.resource = "ceg/símbolos.dic"


    def generateFileContent(self):

        filePath = contentCache.downloadFileIfNeededAndGetLocalPath(pdfUrl)
        pdfParser = PdfParser(filePath)

        entries = {}
        parsingStage = 0

        for line in pdfParser.lines():

            line = line.strip()
            if not line:
                continue

            if parsingStage == 0:
                if line == "SÍMBOLOS:":
                    parsingStage += 1
                continue

            elif parsingStage == 1:
                if line == "CASOS ESPECIAIS:":
                    break

            if line.isdigit():
                continue

            parts = line.split(":")
            comment = parts[0].strip()
            entry = ":".join(parts[1:]).strip()
            if comment in ["FM"]: # Entradas invertidas.
                temporary = comment
                comment = entry
                entry = temporary
            if "," in entry:
                for subentry in entry.split(","):
                    entries[subentry.strip()] = comment
            else:
                entries[entry] = comment


        dictionary  = "# Relación de símbolos máis frecuentes\n"
        dictionary += "# {}\n".format(pdfUrl)
        dictionary += "\n"
        for entry in formatEntriesAndCommentsForDictionary(entries, "símbolo"):
            dictionary += entry
        return dictionary


def loadGeneratorList():
    generators = []
    generators.append(AbbreviationsGenerator())
    generators.append(AcronymsGenerator())
    generators.append(SymbolsGenerator())
    return generators