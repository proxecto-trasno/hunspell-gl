# -*- coding:utf-8 -*-



import codecs, os, sys

import common


def createFoldersIfNeeded(path):
    try:
        os.makedirs(path)
    except:
        pass


def output(text):
    print(text, end="")
    sys.stdout.flush()


def writeToResource(content, resource):
    targetPath = os.path.join(common.getModulesSourcePath(), resource)
    createFoldersIfNeeded(os.path.dirname(targetPath))
    with codecs.open(targetPath, "w", "utf-8") as fileObject:
        fileObject.write(content)


class Generator(object):

    def generateFileContent(self):
        raise Exception("Abstract method")


    def run(self):
        writeToResource(self.generateFileContent(), self.resource)


tupleOfWordsToIgnore = (

    # Símbolos.
    "+", "=", "&", ",", ".", ";", ":",

    # Nexos comúns.
    "A", "As", "O", "Os",
    "Á", "Ás", "Ao", "Aos", "Ó", "Ós",
    "Da", "Das", "De", "Do", "Dos",
    "E",
    "En", "Na", "Nas", "No", "Nos",
    "Entre",
    "Para",
    "Pola", "Polas", "Polo", "Polos", "Por",
    "Tras",
    "Un", "Unha", "Unhas", "Uns",

    # Outros termos comúns correctos en galego.
    "Abadía", "Abadías",
    "Abaixo",
    "Abril",
    "Acción", "Accións",
    "Administración", "Administracións",
    "Agosto",
    "Agrupación",
    "Aldea", "Aldeas",
    "Alianza", "Alianzas",
    "Alta", "Altas", "Alto", "Altos",
    "Alternativa", "Alternativas", "Alternativo", "Alternativos",
    "Alzamento", "Alzamentos",
    "Ano", "Anos",
    "Arquipélago", "Arquipélagos",
    "Arrecife", "Arrecifes",
    "Arriba",
    "Arroio", "Arroios",
    "Antiga", "Antigas", "Antigo", "Antigos",
    "Asasinato", "Asasinatos",
    "Asociación", "Asociacións",
    "Asunto", "Asuntos",
    "Ataque", "Ataques",
    "Atentado", "Atentados",
    "Atol", "Atois",
    "Australiana", "Australianas", "Australiano", "Australianos",
    "Autónoma", "Autónomas", "Autónomo", "Autónomos",
    "Axencia", "Axencias",
    "Baía", "Baías",
    "Baixa", "Baixas", "Baixo", "Baixos",
    "Baldaquino", "Baldaquinos",
    "Baloncesto",
    "Balonmán",
    "Barrio", "Barrios",
    "Basílica", "Basílicas",
    "Batalla", "Batallas",
    "Bieita", "Bieitas", "Bieito", "Bieitos",
    "Blanca", "Blancas", "Blanco", "Blancos",
    "Bloque", "Bloques",
    "Branca", "Brancas", "Branco", "Brancos",
    "Británica", "Británicas", "Británico", "Británicos",
    "Beira", "Beiras",
    "Cabeza", "Cabezas",
    "Cabo", "Cabos",
    "Camiño", "Camiños",
    "Campionato", "Campionatos",
    "Campo", "Campos",
    "Canal", "Canais",
    "Candidatura", "Candidaturas",
    "Canle", "Canles",
    "Capela", "Capelas",
    "Casa", "Casas",
    "Castelo", "Castelos",
    "Castilla",
    "Castiñeiro", "Castiñeiros",
    "Castro", "Castros",
    "Catedral", "Catedrais",
    "Católica", "Católicas", "Católico", "Católicos",
    "Central", "Centrais",
    "Centro", "Centros",
    "Cidade", "Cidades",
    "Cima", "Cimas",
    "Civilización", "Civilizacións",
    "Colexiata", "Colexiatas",
    "Colonia", "Colonias",
    "Comarca", "Comarcas",
    "Comercial", "Comerciais",
    "Comunista", "Comunistas",
    "Concello", "Concellos",
    "Condado", "Condados",
    "Confederación", "Confederacións",
    "Continental", "Continentais", # «Portugal continental».
    "Continente", "Continentes",
    "Convento", "Conventos",
    "Copa", "Copas",
    "Cordilleira", "Cordilleiras",
    "Coroa", "Coroas",
    "Costa", "Costas",
    "Coto", "Cotos",
    "Couto", "Coutos",
    "Cova", "Covas",
    "Cruceiro", "Cruceiros",
    "Cruz", "Cruces",
    "Decembro",
    "Demócrata", "Demócratas",
    "Democrática", "Democráticas", "Democrático", "Democráticos",
    "Delta", "Deltas",
    "Departamento", "Departamentos",
    "Deserto", "Desertos",
    "Día", "Días",
    "Distrito", "Distritos",
    "División", "Divisións",
    "Ducado", "Ducados",
    "Enseada", "Enseadas",
    "Era", "Eras",
    "Ermida", "Ermidas",
    "España",
    "Española", "Españolas", "Español", "Españois",
    "Estado", "Estados",
    "Estreito", "Estreitos",
    "Estrela", "Estrelas",
    "Europea", "Europeas", "Europeo", "Europeos",
    "Exterior", "Exteriores",
    "Facenda",
    "Fachada", "Fachadas",
    "Faro", "Faros",
    "Febreiro",
    "Federación", "Federacións",
    "Federada", "Federadas", "Federado", "Federados",
    "Federal", "Federais",
    "Feira", "Feiras",
    "Fonte", "Fontes",
    "Fórmula", "Fórmulas",
    "Fútbol",
    "Galega", "Galegas", "Galego", "Galegos",
    "Galicia", "Galiza",
    "Golfo", "Golfos",
    "Gran", "Grande", "Grandes",
    "Groba", "Grobas",
    "Home", "Homes",
    "Idade", "Idades",
    "Igrexa", "Igrexas",
    "Illa", "Illas",
    "Illote", "Illotes",
    "Imperio", "Imperios",
    "Incidente", "Incidentes",
    "Índice", "Índices",
    "Insua", "Insuas",
    "Intelixencia", "Intelixencias",
    "Interior", "Interiores",
    "Interna", "Internas", "Interno", "Internos",
    "Internacional", "Internacionais",
    "Islámica", "Islámicas", "Islámico", "Islámicos",
    "Leste",
    "Liberación", "Liberacións",
    "Liberal", "Liberais",
    "Libre", "Libres",
    "Liga", "Ligas",
    "Litoral", "Litorais",
    "Lombo", "Lombos",
    "Lugar", "Lugares",
    "Madeira", "Madeiras",
    "Maio",
    "Maior", "Maiores",
    "Mar", "Mares",
    "Marzo",
    "Masacre", "Masacres",
    "Matanza", "Matanzas",
    "Macizo", "Macizos",
    "Menor", "Menores",
    "Meridional", "Meridionais",
    "Mes", "Meses",
    "Metro", "Metros",
    "Ministerio", "Ministerios",
    "Montaña", "Montañas",
    "Monte", "Montes",
    "Mosteiro", "Mosteiros",
    "Mundial", "Mundiais",
    "Muller", "Mulleres",
    "Nacional", "Nacionais",
    "Nacionalista", "Nacionalistas",
    "Negra", "Negras", "Negro", "Negros",
    "Nosa", "Nosas", "Noso", "Nosos",
    "Nova", "Novas", "Novo", "Novos",
    "Novembro",
    "Norte",
    "Occidental", "Occidentais",
    "Océano", "Océanos",
    "Oeste",
    "Oliveira", "Oliveiras",
    "Operación", "Operacións",
    "Oriental", "Orientais",
    "Outeiro", "Outeiros",
    "Outubro",
    "Pacto", "Pactos",
    "País", "Países",
    "Parada", "Paradas",
    "Partido", "Partidos",
    "Pazo", "Pazos",
    "Pena", "Penas",
    "Península", "Penínsulas",
    "Pequena", "Pequenas", "Pequeno", "Pequenos",
    "Perla", "Perlas",
    "Pico", "Picos",
    "Pobo", "Pobos",
    "Ponte", "Pontes",
    "Popular", "Populares",
    "Porta", "Portas",
    "Pórtico", "Pórticos",
    "Porto", "Portos",
    "Prado", "Prados",
    "Praia", "Praias",
    "Praza", "Prazas",
    "Princesa", "Princesas", "Príncipe", "Príncipes",
    "Principado", "Principados",
    "Progresista", "Progresistas",
    "Protesta", "Protestas",
    "Provincia", "Provincias",
    "Pública", "Públicas", "Público", "Públicos",
    "Radical", "Radicais",
    "Radio", "Radios",
    "Raíña", "Raíñas", "Rei", "Reis",
    "Real", "Reais",
    "Rebelión", "Rebelións",
    "Refuxio", "Refuxios",
    "Regato", "Regatos",
    "Rego", "Regos",
    "Regueiro", "Regueiros",
    "Reino", "Reinos",
    "Reitoral", "Reitorais",
    "Remedio", "Remedios",
    "República", "Repúblicas",
    "Republicana", "Republicanas", "Republicano", "Republicanos",
    "Rexión", "Rexións",
    "Ría", "Rías",
    "Ribeira", "Ribeiras",
    "Río", "Ríos",
    "Rúa", "Rúas",
    "Ruína", "Ruínas",
    "San", "Santa", "Santas", "Santo", "Santos",
    "Sanidade",
    "Santuario", "Santuarios",
    "Secreta", "Secretas", "Secreto", "Secretos",
    "Secuestro", "Secuestros",
    "Señor", "Señora", "Señoras", "Señores",
    "Señorío", "Señoríos",
    "Serra", "Serras",
    "Servizo", "Servizos",
    "Setembro",
    "Silva", "Silvas",
    "Sistema", "Sistemas",
    "Socialista", "Socialistas",
    "Sol", "Soles",
    "Souto", "Soutos",
    "Soviética", "Soviéticas", "Soviético", "Soviéticos",
    "Subrexión", "Subrexións",
    "Sur",
    "Televisión", "Televisións",
    "Templo", "Templos",
    "Territorial", "Territoriais",
    "Tiroteo", "Tiroteos",
    "Torre", "Torres",
    "Tradicionalista", "Tradicionalistas",
    "Unida", "Unidas", "Unido", "Unidos",
    "Unión", "Unións",
    "Val", "Vales",
    "Veiga", "Veigas",
    "Vella", "Vellas", "Vello", "Vellos",
    "Verde", "Verdes",
    "Vila", "Vilas",
    "Vilar", "Vilares",
    "Virxe", "Virxes",
    "Xadrez",
    "Xaneiro",
    "Xenocidio", "Xenocidios",
    "Xogo", "Xogos",
    "Xuño",
    "Xullo",

    # Numerais.
    "Un", "Unha", "Unhas", "Uns",
    "Dous", "Dúas",
    "Tres", # «Penedo dos Tres Reinos»
    "Catro", # «Pena dos Catro Cabaleiros»
    "Cinco",
    "Seis",
    "Sete",
    "Oito",
    "Nove",
    "Dez",

    # Ordinais. Por exemplo, «Cuarta República».
    "Primeira", "Primeiras", "Primeiro", "Primeiros",
    "Segunda", "Segundas", "Segundo", "Segundos",
    "Terceira", "Terceiras", "Terceiro", "Terceiros",
    "Cuarta", "Cuartas", "Cuarto", "Cuartos",
    "Quinta", "Quintas", "Quinto", "Quintos",
    "Sexta", "Sextas", "Sexto", "Sextos",
    "Sétima", "Sétimas", "Sétimo", "Sétimos",
    "Oitava", "Oitavas", "Oitavo", "Oitavos",
    "Novena", "Novenas", "Noveno", "Novenos",
    "Décima", "Décimas", "Décimo", "Décimos",

    # Multiplicadores. Por exemplo: «Cuádrupla Alianza».
    "Dupla", "Duplas", "Cuádruplo", "Cuádruplos",
    "Tripla", "Triplas", "Triplo", "Triplos",
    "Cuádrupla", "Cuádruplas", "Cuádruplo", "Cuádruplos",
    "Quíntupla", "Quíntuplas", "Quíntuplo", "Quíntuplos",
    "Séxtupla", "Séxtuplas", "Séxtuplo", "Séxtuplos",
    "Séptupla", "Séptuplas", "Séptuplo", "Séptuplos",

    # Números romanos.
    "I", "II", "III", "IV", "V", "VI", "VI", "VII", "VIII", "X",
)
wordsToIgnore = set()
for word in tupleOfWordsToIgnore:
    wordsToIgnore.add(word)
    wordsToIgnore.add(word.lower())