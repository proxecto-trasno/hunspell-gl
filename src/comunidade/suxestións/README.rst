As suxestións da comunidade están divididas en ficheiros segundo a fonte que
apoia a substitución a nivel de significado.

Por exemplo, para «apilar» existen dúas suxestións: a palabra «encastelar» está
en «rag.rep» porque o Dicionario da Real Academia Galega a recolle con ese
significado similar ao do castelán «apilar», pero «rimar» está en
«comunidade.rep» porque o Dicionario da Real Academia Galega non a recolle con
ningún significado relacionado.
