import re
from collections import defaultdict

from scrapy import Request

from shared.scrapy import BaseSpider, log_debug_request
from shared.scrapy import Policy as BasePolicy
from shared.gl import definitions_to_pos
from shared.text import normalize_space


UNACUTE = {
    "ó": "o",
}

LEMMA_FIXES = {
    "aminoacil-RNA<sub> t</sub>": "aminoacil-RNAₜ",
    "aminoacilo-RNA<sub> t</sub> -sintetase": "aminoacilo-RNAₜ-sintetase",
}


def parse_lemmas(title, response):
    lemma, feminine_lemma = title, None
    lemma = lemma.replace("–", "-")
    lemma = re.sub(r"\D\s*\d+\s*$", "", lemma)  # p. ex. «canear  1» → «canear»
    if lemma.endswith("*"):
        # Visto en palabras latinas e éuscaras (p. ex. «abertzale»).
        lemma = lemma[:-1]
    elif lemma.endswith("!"):
        # Visto en interxeccións (p. ex. «abur!»).
        lemma = lemma[:-1]
    elif lemma in LEMMA_FIXES:
        lemma = LEMMA_FIXES[lemma]
    if ", " in lemma:
        # p. ex. «desbarate, a/ao, ó», «carranchapernas, a/ás»
        if lemma.endswith(", a/ao, ó") or lemma.endswith(", a/ás"):
            parts = lemma.split(", ", maxsplit=1)
            lemma = f"a {parts[0]}"
        # p. ex. «carricho, ao/ó»
        elif lemma.endswith(", ao/ó"):
            parts = lemma.split(", ", maxsplit=1)
            lemma = f"ao {parts[0]}"
        # p. ex. «ao, á»
        elif not any(
            "locución" in po
            for po in pos_from_selector_list(
                response.xpath(
                    "//span[contains(@class, 'xeslin-detalle-acepcions-acepcion')]"
                )
            )
        ):
            lemma, feminine_lemma = lemma.split(", ", maxsplit=1)
        # p. ex. «agacho, ao»
        else:
            parts = lemma.split(", ", maxsplit=1)
            lemma = f"{parts[1]} {parts[0]}"
    if lemma == "automotor -ra/-triz":
        return "automotor", {"automotora", "automotriz"}
    if " -" in lemma or lemma.endswith("-na"):  # p. ex. «adamantino-na»
        # p. ex. «abaetado -da»
        lemma, feminine_suffix = lemma.rsplit("-", maxsplit=1)
        lemma, feminine_suffix = lemma.strip(), feminine_suffix.strip()
        if lemma.endswith("ón") and feminine_suffix == "na":
            # p. ex. «abusón -na»
            feminine_lemma = (
                lemma[: -len(feminine_suffix)]
                + UNACUTE[lemma[-len(feminine_suffix)]]
                + feminine_suffix
            )
        elif lemma.endswith("de") and feminine_suffix == "sa":
            # p. ex. «alcaide -sa»
            feminine_lemma = lemma + feminine_suffix
        elif lemma.endswith("el") and feminine_suffix == "la":
            # p. ex. «aquel -la»
            feminine_lemma = lemma[:-1] + feminine_suffix
        else:
            feminine_lemma = lemma[: -len(feminine_suffix)] + feminine_suffix
    parts = lemma.split(" ")
    if len(parts) == 2:
        # p. ex. «apendiculario ria»
        if parts[1].endswith("a") and parts[0].endswith(parts[1][:-1] + "o"):
            lemma = parts[0]
            feminine_lemma = parts[0][: -len(parts[1])] + parts[1]
    return lemma, {feminine_lemma} if feminine_lemma else set()


POS_MAP = {
    "adv": "adverbio",
    "adx": "adxectivo",
    "adx f": "adxectivo feminino",
    "adx m": "adxectivo masculino",
    "art det": "artigo determinado",
    "art indet": "artigo indeterminado",
    "contr": "contracción",
    "conx": "conxunción",
    "cuant": "cuantitativo",
    "dem": "demostrativo",
    "dem n": "demostrativo neutro",
    "exclam": "exclamativo",
    "indef": "indefinido",
    "indef sing": "indefinido singular",
    "indef pl": "indefinido plural",
    "int": "interrogativo",
    "interx": "interxección",
    "num card": "numeral cardinal",
    "num ord": "numeral ordinal",
    "loc adv": "locución adverbial",
    "loc conx": "locución conxuntiva",
    "loc prep": "locución preposicional",
    "p irreg": "participio irregular",
    "p reg": "participio regular",
    "pos": "posesivo",
    "prep": "preposición",
    "pron": "pronome",
    "pron pers": "pronome persoal",
    "pron rel": "pronome relativo",
    "s": "substantivo",
    "s pl": "substantivo plural",
    "s f": "substantivo feminino",
    "s f pl": "substantivo feminino plural",
    "s m": "substantivo masculino",
    "s m pl": "substantivo masculino plural",
    "v i": "verbo intransitivo",
    "v pron": "verbo pronominal",
    "v t": "verbo transitivo",
}
_IGNORED_POS = {
    "pref",
    "v",
}


def pos_from_selector_list(selector_list):
    result = set()
    for entry in selector_list:
        pos = entry.xpath(
            "preceding-sibling::*[contains(@class, 'gramatical')]/text()"
        ).get()
        if pos:
            pos = normalize_space(pos)
        if not pos:
            continue
        po_list = [normalize_space(po) for po in pos.split(",")]
        po_list = [po for po in po_list if po]
        if not po_list:
            continue
        if len(po_list) == 1 and po_list[0] in _IGNORED_POS:
            continue
        gender = entry.xpath(
            "preceding-sibling::*[contains(@class, 'xenero')]/text()"
        ).get()
        if gender:
            gender = normalize_space(gender)
            if gender:
                # Só asignamos o xénero á última categoría gramatical da lista
                # (p. ex. «acuifoliáceo»).
                po_list[-1] = f"{po_list[-1]} {gender}"
        for po in po_list:
            if (
                po != po_list[0]
                and po_list[0].startswith("v ")
                and not po.startswith("v ")
            ):
                po = f"v {po}"
            po = re.sub(r" (?:col|fig|p ext)$", "", po)
            try:
                result.add(POS_MAP[po])
            except KeyError:
                continue
    return result


ENTRY_XPATH = "span[contains(@class, 'xeslin-detalle-acepcions-acepcion')]"
SUBENTRIES_XPATH = "*[contains(@class, 'xeslin-detalle-acepcions-subacepcions')]"


def parse_pos(response, lemma, feminine_lemmas):
    result = set()
    # Primeiro obtemos a categoría gramatical excluíndo as acepcións aniñadas,
    # que non inclúen o xénero (p. ex. «absceso»).
    xpath = f"//{ENTRY_XPATH}[not(.//ancestor::{SUBENTRIES_XPATH})]"
    result |= pos_from_selector_list(response.xpath(xpath))
    # E tamén a categoría gramatical de acepcións aniñadas cando a
    # categoría en si non está aniñada (p. ex. «acanalado», «alimento»).
    xpath = f"//{SUBENTRIES_XPATH}"
    result |= pos_from_selector_list(response.xpath(xpath))
    # Se non hai categorías gramaticais que non estean aniñadas (pode que todas
    # estean aniñadas, p. ex. «abisinio»), ou se a entrada indica forma
    # feminina (p. ex. «abkhazo»), obtermos as categorías gramaticais aniñadas.
    if not result or feminine_lemmas:
        xpath = f"//{ENTRY_XPATH}[.//ancestor::{SUBENTRIES_XPATH}]"
        result |= pos_from_selector_list(response.xpath(xpath))
    # Se non hai categorías gramaticais, nin non aniñadas nin aniñadas,
    # determinamos as categorías gramaticais segundo as definicións.
    if not result:
        css = ".xeslin-detalle-acepcions-acepcion ::text"
        definitions = response.css(css).getall()
        result |= definitions_to_pos(definitions, response.url)
    # p. ex. «ad calendas»
    if not result and lemma.startswith("ad "):
        result.add("adverbio")
    # p. ex. «ADN»
    if not result and len(lemma) > 1 and lemma.isupper():
        result.add("sigla")
    return result


def parse_conjugation(response):
    if not response.css(".xeslin-detalle-conxugar"):
        return None

    def _parse_conjugation(mode, time, person):
        mode_div = response.xpath(f"//h3[text()='{mode}']/following-sibling::*")
        times_xpath = (
            f".//*"
            f"[@class='xeslin-detalle-termo-conxugacion-titulo col-xs-12']"
            f"[text()='{time}']"
            f"/following-sibling::*"
            f"[contains(@class, 'xeslin-detalle-conxugar-item-texto')]"
            f"/text()"
        )
        times = mode_div.xpath(times_xpath).getall()
        return times[person - 1].strip()

    return {
        "i_p_p1": _parse_conjugation("Indicativo", "Presente", 1),
        "i_p_p3": _parse_conjugation("Indicativo", "Presente", 3),
        "i_p_p4": _parse_conjugation("Indicativo", "Presente", 4),
        "i_p_p6": _parse_conjugation("Indicativo", "Presente", 6),
        "s_p_p4": _parse_conjugation("Subxuntivo", "Presente", 4),
    }


def parse_nested_pos(response, word):
    result = defaultdict(set)
    # «Tm s», p. ex. en «absentista».
    xpath = f"//{ENTRY_XPATH}"
    for selector in response.xpath(xpath):
        html = selector.get()
        pos = pos_from_selector_list([selector])
        if len(pos) != 1:
            continue
        parent = next(iter(pos))
        for k, v in POS_MAP.items():
            if f". Tm {k}." not in html:
                continue
            for gender in ("feminino", "masculino"):
                if not parent.endswith(f" {gender}") or v.endswith(f" {gender}"):
                    continue
                # p. ex. «acataléctico» (adxectivo → adxectivo masculino)
                v = f"{v} {gender}"
                break
            result[parent] = {v}
            break
    return result


def parse_same_as(response):
    css = (
        ".xeslin-detalle-acepcions-acep-xenero::text, "
        ".xeslin-detalle-acepcions-acep-gramatical::text"
    )
    has_parts_of_speech = any(content.strip() for content in response.css(css).getall())
    if has_parts_of_speech:
        return None
    xpath = (
        "//*"
        "[@class='xeslin-detalle-acepcions-acepcion']"
        "[not(preceding-sibling::*[@class='xeslin-detalle-acepcions-acep-subentrada'])]"
        "/p[not(text())]"
        "/em/text()"
    )
    same_as = response.xpath(xpath).get()
    if same_as is None:
        return None
    same_as = same_as.replace(".", "").strip()
    if not same_as or " " in same_as:
        return None
    return same_as


def _is_empty_index(response):
    return bool(
        response.xpath(
            "//div[@class='block-content']"
            "//text()[contains(., 'Termo non atopado.')]"
        )
    )


class Policy(BasePolicy):
    def is_cached_response_valid(self, cachedresponse, response, request):
        return super().is_cached_response_valid(
            cachedresponse, response, request
        ) and not _is_empty_index(response)


class Spider(BaseSpider):
    name = "digalego"

    custom_settings = {
        **BaseSpider.custom_settings,
        "HTTPCACHE_POLICY": Policy,
        "USER_AGENT": (
            "Mozilla/5.0 (X11; Linux x86_64; rv:121.0) Gecko/20100101 Firefox/121.0"
        ),
    }

    def entry_request(self, index):
        return Request(
            f"https://digalego.xunta.gal/gl/termo/{index}",
            callback=self.parse_entry,
        )

    def debug_request(self):
        return Request(
            f"https://digalego.xunta.gal/gl/busca?palabra={self.debug_word}&busca_opcions=completa&idiomas=none&op=Buscar",
            callback=self.parse_entry,
        )

    def start_requests(self):
        if self.debug_word:
            yield self.debug_request()
            return
        yield self.entry_request(1)

    @log_debug_request
    def parse_entry(self, response):
        match = re.search(r"/termo/(\d+)", response.url)
        if match:
            index = int(match[1])
        elif self.debug_word:
            raise NotImplementedError(
                f"Está a depurar unha palabra, «{self.debug_word}», que ao "
                f"buscala no dicionario devolve varias entradas. O código "
                f"aínda non está preparado para este caso."
            )
        else:
            raise NotImplementedError(
                f"O URL da resposta ({response.url}) non ten índice."
            )

        if _is_empty_index(response):
            self.logger.info(f"Chegouse a un índice baleiro: {index}")
            return

        if not self.debug_word:
            yield self.entry_request(index + 1)

        word = response.css("h1::text").get()
        if word.startswith("-") or word.endswith("-"):
            return

        lemma, feminine_lemmas = parse_lemmas(word, response)
        conjugation = parse_conjugation(response)
        parts_of_speech = parse_pos(response, lemma, feminine_lemmas)
        nested_parts_of_speech = parse_nested_pos(response, word)
        for parent, pos in nested_parts_of_speech.items():
            parts_of_speech |= pos
        same_as = parse_same_as(response)

        entry = {
            "lemma": lemma,
            "parts_of_speech": parts_of_speech,
            "feminine_lemmas": feminine_lemmas,
            "nested_parts_of_speech": nested_parts_of_speech,
            "same_as": same_as,
        }
        if conjugation:
            entry["conjugation"] = conjugation

        yield entry
