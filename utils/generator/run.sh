if [ ! -d "$DIRECTORY" ]; then
    python3 -m venv venv
    . venv/bin/activate
    pip install pip-tools
else
    . venv/bin/activate
fi
pip-sync
LC_COLLATE=gl_ES.UTF-8 \
    python \
    -W ignore::UserWarning:scrapy.core.scraper:210 \
    generate.py \
    $1
