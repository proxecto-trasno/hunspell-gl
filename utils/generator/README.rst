=========
Generator
=========

Ferramenta para xerar a partir de datos públicos de Internet versións
alternativas de ficheiros fonte, para poder detectar facilmente se necesitan
cambios.

Pode usarse para ``src/rag/gl/correcto.dic``.

Requisitos
==========

-   Un sistema Linux configurado en galego

-   Python 3

-   ``sort`` (de `GNU Coreutils <https://www.gnu.org/software/coreutils/>`_)


Uso
===

En ``utils/generator/``, executa::

    ./run.sh drag

Para evitar sobrecargar o servidor da RAG, o proceso manda as consultas unha a
unha, e deixa un tempo entre consultas que varía segundo a velocidade coa que
está a responder o servidor. Debido a iso, o proceso inicial pode durar moitas
horas. Pódese interromper o proceso (Ctrl+C) para retomalo máis tarde; o
contido xa descargado queda gardado nunha caché e non se volverá descargar ata
que se elimine a caché.

Este proceso xera o ficheiro ``src/rag/gl/correcto.dic~``. A continuación
débese comparar con ``src/rag/gl/correcto.dic`` en busca de discrepancias. Por
exemplo podes usar ``diff`` en ``src/rag/gl/``::

    diff -Naur correcto.dic correcto.dic~ | less

Deterse en cada diferenza:

-   Se o cambio é correcto, aplicalo manualmente a ``src/rag/gl/correcto.dic``.

-   Se o cambio é incorrecto, corrixir o código responsable en
    ``utils/generator/``, e xerar ``src/rag/gl/correcto.dic~`` de novo.

    Podes usar a variable de ambiente ``DEBUG_WORD`` para definir unha palabra
    concreta sobre a que estás a depurar e obter información adicional sobre o
    procesamento relacionado coa palabra, para identificar a raíz do problema
    máis facilmente::

        DEBUG_WORD=palabra ./run.sh drag

    Que facer unha vez solucionado o problema depende de onde estivese:

    -   Se a corrección foi en código que se executa despois da xeración de
        ``cache/drag.jl``, como por exemplo en ``drag/processor.py``, en
        ``drag/builder.py`` ou en ``drag/fixes.yml``, executa ``./run.sh`` de
        novo.

    -   Se o problema está en que os datos de ``cache/drag.jl`` son
        incorrectos (p. ex. a información de categoría gramatical,
        ``parts_of_speech``, é incorrecta), pero os datos están ben no DRAG,
        soluciona o problema en ``drag/spider.py``, garda ``cache/drag.jl`` con
        outro nome por se o necesitas máis tarde (p. ex. ``cache/drag.jl~``) e
        executa ``./run.sh`` de novo.

        Unha vez solucionado o problema, ti decides se paga a pena executar
        ``./run.sh`` de novo desde cero ou cambiar o nome de ``cache/drag.jl~``
        de volta a ``cache/drag.jl`` e modificar o seu contido manualmente.

        Xerar ``cache/drag.jl`` desde cero, unha vez as páxinas do DRAG están
        todas na caché, pode levar arredor de 20 minutos, dependendo da túa
        CPU e disco.


Caché
=====

A ferramenta crea un cartafol, ``cache``, onde se gardarán:

-   Un cartafol, ``scrapy``, onde se almacena unha caché das páxinas do DRAG
    visitadas.

    Isto permite:

    -   Deter a descarga das páxinas e continuala sen necesidade de descargar
        de novo as páxinas que xa se descargaran, acelerando así o seu
        procesamento.

    -   Repetir o procesamento de todas as páxinas, por exemplo tras cambiar a
        lóxica de procesamento, sen necesidade de descargar de novo todas as
        páxinas, acelerando así o seu procesamento e evitando unha carga
        innecesaria no servidor da RAG.

-   Un ficheiro, ``drag.jl``, en formato JSON Lines, co resultado de procesar
    as páxinas do DRAG extraendo os datos necesarios para a xeración do
    ficheiro de Hunspell final.

    Isto permite repetir a xeración do ficheiro de Hunspell final sen
    necesidade de procesar de novo as páxinas do DRAG.

Existen circunstancias que requiren eliminar todo ou parte do contido do
cartafol ``cache``:

-   Para descargar de novo os últimos datos do DRAG hai que eliminar todo o
    contido de ``cache``.

-   Tras cambios na forma de xerar o ficheiro ``drag.jl``, este ficheiro debe
    primeiro eliminarse para poder xeralo de novo a partir das páxinas do DRAG.
