def conjugate(infinitive):
    """Given the infinitive form of a verb, return the most likely conjugation.

    The resulting conjugation is not complete, it is limited to those forms
    that are required to assign the right SFX rules to the infinitive:
    indicative present P1, P3, P4, and P6, and subjunctive present P4.
    """
    if infinitive.endswith("ar"):
        if infinitive.endswith("car"):
            base = infinitive[:-3]
            return {
                "i_p_p1": f"{base}co",
                "i_p_p3": f"{base}ca",
                "i_p_p4": f"{base}camos",
                "i_p_p6": f"{base}can",
                "s_p_p4": f"{base}quemos",
            }
        if infinitive.endswith("gar"):
            base = infinitive[:-3]
            return {
                "i_p_p1": f"{base}go",
                "i_p_p3": f"{base}ga",
                "i_p_p4": f"{base}gamos",
                "i_p_p6": f"{base}gan",
                "s_p_p4": f"{base}guemos",
            }
        if infinitive.endswith("guar"):
            base = infinitive[:-4]
            return {
                "i_p_p1": f"{base}guo",
                "i_p_p3": f"{base}gua",
                "i_p_p4": f"{base}guamos",
                "i_p_p6": f"{base}guan",
                "s_p_p4": f"{base}güemos",
            }
        if infinitive.endswith("zar"):
            base = infinitive[:-3]
            return {
                "i_p_p1": f"{base}zo",
                "i_p_p3": f"{base}za",
                "i_p_p4": f"{base}zamos",
                "i_p_p6": f"{base}zan",
                "s_p_p4": f"{base}cemos",
            }
        base = infinitive[:-2]
        return {
            "i_p_p1": f"{base}o",
            "i_p_p3": f"{base}a",
            "i_p_p4": f"{base}amos",
            "i_p_p6": f"{base}an",
            "s_p_p4": f"{base}emos",
        }
    if infinitive.endswith("er"):
        if infinitive.endswith("cer"):
            base = infinitive[:-3]
            return {
                "i_p_p1": f"{base}zo",
                "i_p_p3": f"{base}ce",
                "i_p_p4": f"{base}cemos",
                "i_p_p6": f"{base}cen",
                "s_p_p4": f"{base}zamos",
            }
        if infinitive.endswith("guer"):
            base = infinitive[:-4]
            return {
                "i_p_p1": f"{base}go",
                "i_p_p3": f"{base}gue",
                "i_p_p4": f"{base}guemos",
                "i_p_p6": f"{base}guen",
                "s_p_p4": f"{base}gamos",
            }
        if infinitive.endswith("quer"):
            base = infinitive[:-4]
            return {
                "i_p_p1": f"{base}co",
                "i_p_p3": f"{base}que",
                "i_p_p4": f"{base}quemos",
                "i_p_p6": f"{base}quen",
                "s_p_p4": f"{base}camos",
            }
        base = infinitive[:-2]
        return {
            "i_p_p1": f"{base}o",
            "i_p_p3": f"{base}e",
            "i_p_p4": f"{base}mos",
            "i_p_p6": f"{base}en",
            "s_p_p4": f"{base}amos",
        }
    if infinitive.endswith("ir"):
        if infinitive.endswith("cir"):
            base = infinitive[:-3]
            return {
                "i_p_p1": f"{base}zo",
                "i_p_p3": f"{base}ce",
                "i_p_p4": f"{base}cimos",
                "i_p_p6": f"{base}cen",
                "s_p_p4": f"{base}zamos",
            }
        if infinitive.endswith("guir"):
            base = infinitive[:-4]
            return {
                "i_p_p1": f"{base}go",
                "i_p_p3": f"{base}gue",
                "i_p_p4": f"{base}guimos",
                "i_p_p6": f"{base}guen",
                "s_p_p4": f"{base}gamos",
            }
        if infinitive.endswith("quir"):
            base = infinitive[:-4]
            return {
                "i_p_p1": f"{base}co",
                "i_p_p3": f"{base}que",
                "i_p_p4": f"{base}quimos",
                "i_p_p6": f"{base}quen",
                "s_p_p4": f"{base}camos",
            }
        base = infinitive[:-2]
        return {
            "i_p_p1": f"{base}o",
            "i_p_p3": f"{base}e",
            "i_p_p4": f"{base}mos",
            "i_p_p6": f"{base}en",
            "s_p_p4": f"{base}amos",
        }
    if infinitive.endswith("uír"):
        base = infinitive[:-3]
        return {
            "i_p_p1": f"{base}úo",
            "i_p_p3": f"{base}úe",
            "i_p_p4": f"{base}uímos",
            "i_p_p6": f"{base}úen",
            "s_p_p4": f"{base}uamos",
        }
    return None
