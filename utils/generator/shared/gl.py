import re
from functools import partial

from icu import Collator, Locale

from shared.text import normalize_space

COLLATOR = Collator.createInstance(Locale("gl_ES.UTF-8"))

gl_sorted = partial(sorted, key=lambda v: COLLATOR.getSortKey("  ".join(v)))


GENDER_POS = {
    "adxectivo",
    "artigo determinado",
    "demostrativo",
    "locución substantiva",
    "posesivo",
    "pronome persoal",
    "relativo",
    "substantivo",
}

# Parts of speech that do not allow gender, number or conjugation.
INFLEXIBLE_POS = {
    "abreviatura",
    "adverbio",
    "adverbio latino",
    "adxectivo feminino plural",
    "adxectivo feminino singular",
    "adxectivo masculino plural",
    "adxectivo masculino singular",
    "conxunción",
    "interxección",
    "interxección latina",
    "locución adverbial",
    "locución adverbial latina",
    "locución adxectiva latina",
    "prefixo",
    "preposición",
    "sigla",
    "símbolo",
    "substantivo feminino plural",
    "substantivo feminino singular",
    "substantivo masculino plural",
    "substantivo masculino singular",
    "verbo latino",
}

# Verb parts of speech.
VERB_POS = {
    "verbo intransitivo",
    "verbo pronominal",
    "verbo transitivo",
}

# Parts of speech that allow neither gender nor number.
_NEITHER_GENDER_NOR_NUMBER_POS = INFLEXIBLE_POS | VERB_POS

# Parts of speech that allow no gender.
MASC_POS = {
    "adxectivo masculino",
    "locución substantiva masculina",
    "substantivo masculino",
}
FEM_POS = {
    "adxectivo feminino",
    "locución substantiva feminina",
    "substantivo feminino",
}
GENDER_POS = MASC_POS | FEM_POS
NO_GENDER_POS = GENDER_POS | _NEITHER_GENDER_NOR_NUMBER_POS

# Parts of speech that allow no number.
NO_NUMBER_POS = {
    "adxectivo plural",
    "adxectivo singular",
    "indefinido plural",
    "indefinido singular",
    "substantivo plural",
    "substantivo singular",
}
NO_NUMBER_POS |= _NEITHER_GENDER_NOR_NUMBER_POS

# Parts of speech that allow no gender or number.
NO_GENDER_OR_NUMBER_POS = NO_NUMBER_POS | NO_GENDER_POS


def singularize(plural_lemma):
    if plural_lemma.endswith("s"):
        return plural_lemma[:-1]
    return plural_lemma


_DEF_POS_MAP = {
    "abreviatura": r"(?i)^(?:abreviatura|forma\s+abreviada)",
    "sigla": r"(?i)^sigla",
    "símbolo": r"(?i)^símbolo",
    "substantivo feminino": r"(?i)^(?:calidade\s+de|figura)\s",
    "adxectivo": r"(?i)^(?:\[[^]]+\]\s+)?que\s",
}


def definitions_to_pos(definitions, reference):
    definitions = [normalize_space(definition) for definition in definitions]
    definitions = [definition for definition in definitions if definition]
    result = set()
    for definition in definitions:
        for po, pattern in list(_DEF_POS_MAP.items()):
            if re.search(pattern, definition):
                result.add(po)
    return result
