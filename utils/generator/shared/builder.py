import json
import re
from collections import defaultdict
from enum import Enum
from inspect import getframeinfo, stack
from pathlib import Path
from tempfile import TemporaryDirectory

import sh
from loguru import logger

from shared.debug import DEBUG_WORD
from shared.gl import (
    FEM_POS,
    INFLEXIBLE_POS,
    MASC_POS,
    NO_GENDER_POS,
    NO_GENDER_OR_NUMBER_POS,
    NO_NUMBER_POS,
    gl_sorted,
)
from shared.jsonlines import iter_json_lines


ADJ_10_17_RE = re.compile(
    r"""(?ux)
    (
        an|
        deu|
        mao|
        [nr]u|
        [sv]ó
    )$
    """
)
INFLEX_NUM_RE = re.compile(
    r"(?:un|unha|dous|dúas|tres|catro|cinco|seis|sete|oito|nove|dez|ce|nte|nta|ntos|cen|mil)$"
)
INVARIABLE_SET = {
    "alguén",
    "cada",
    "cadaquén",
    "calquera",
    "daca",
    "demais",
    "eu",
    "máis",
    "ninguén",
    "quenquera",
    "ren",
}

ADJ_10_RE = re.compile(
    r"""(?ux)
    (
        [áéíóú](?!bel)[^\W\daáeéiíïoóuúü]+[aeiou]l|  # p. ex. fráxil
        ^[^\W\daáeéiíïoóuúü]*[aeiou]l|  # p. ex. vil
        cortés|
        a|á|ar|ás|e|í|ín|ior|[^o]r|lor|y|z
    )$
    """
)

SPECIAL_PLURALS = {
    "báner": 11,
    "bemol": 10,
    "brístol": 12,
    "calcapapeis": None,
    "cartafol": 10,
    "carácter": 13,
    "chuchamel": 10,
    "chupamel": 10,
    "cóctel": 12,
    "cuproníquel": 12,
    "decibel": 10,
    "diésel": 12,
    "escornabois": None,
    "fútbol": 12,
    "gángster": 11,
    "gardabois": None,
    "gnomon": 13,
    "hertz": 13,
    "hidromel": 10,
    "hippy": 11,
    "júnior": 11,
    "lavacristais": None,
    "leu": 13,
    "lev": 13,
    "lied": 13,
    "máster": 11,
    "megahertz": 13,
    "mispíquel": 12,
    "náhuatl": 12,
    "níquel": 12,
    "ollomol": 10,
    "perifol": 10,
    "portacedés": None,
    "portapapeis": None,
    "póster": 11,
    "proxy": 11,
    "quilohertz": 13,
    "rally": 11,
    "redefol": 10,
    "rímmel": 12,
    "sénior": 11,
    "topless": None,
    "whisky": 11,
}

CONSONANTE = r"(?:[^\W\daáeéiíïoóuúüy]|qu(?=[ie]))"
SILABA_SEN_TIL = rf"(?:[aeiou]{CONSONANTE}+|[aeo][iu]{CONSONANTE}*)"
P_12_RE = re.compile(
    rf"""(?ux)
    (
        # Polisílabas agudas que rematan en -l
        {SILABA_SEN_TIL}(?:[iu]?[aeo]|[aeo][iu]|i?u|u?i)l
        |[aeo][aeo]l
        |(?<![iu])[aeo][íú]l
        |^{CONSONANTE}*[iu][aeo]l
        # Graves que rematan en -bel
        |[áéíóú]bel
    )$
    """
)
P_NONE_RE = re.compile(
    rf"""(?ux)
    (
        # Polisílabas non agudas que rematan en -s
        {SILABA_SEN_TIL}(?:[iu]?[aeo]|i?u|u?i)s
        |[aeo][aeo]{CONSONANTE}*s
        # Rematadas en -x ou en varias consonantes
        # https://dubidasdogalego.wordpress.com/2012/01/13/cal-e-o-plural-de-fax/
        |[np]s
        |x
        # Díxitos, p. ex. covid-19
        |\d
        # lapis e -lapis
        |lapis
    )$
    """
)


def get_plural_rule(lemma):
    lemma = lemma.split(" de ", maxsplit=1)[0]
    if lemma in SPECIAL_PLURALS:
        return SPECIAL_PLURALS[lemma]
    if P_12_RE.search(lemma):
        return 12
    if P_NONE_RE.search(lemma) or lemma[0].isupper():
        return None
    return 10


def get_feminine_rule(masculine, feminine):
    masculine = masculine.split(" de ", maxsplit=1)[0]
    feminine = feminine.split(" de ", maxsplit=1)[0]

    def sfx(remove, add, endswith=""):
        endswith += remove
        if endswith and not re.search(f"{endswith}$", masculine):
            return False
        prefix = masculine[: -len(remove)] if remove else masculine
        return f"{prefix}{add}" == feminine

    if sfx("", "a"):
        return 14
    if (
        sfx("o", "a")
        or sfx("ó", "á")
        or sfx("e", "a")
        or sfx("án", "á")
        or sfx("ón", "oa")
        or sfx("ún", "ua")
        or sfx("ín", "ina")
        or sfx("és", "esa")
        or sfx("or", "riz", "t")
        or sfx("dor", "triz")
    ):
        return 15
    if sfx("án", "ana") or sfx("ón", "ona") or sfx("ao", "á"):
        return 16
    if (
        sfx("ao", "á", "m")
        or sfx("an", "a", "[sv]")
        or sfx("ó", "oa", "[sv]")
        or sfx("ón", "a")
        or sfx("u", "úa", "[rn]")
        or sfx("eu", "ía", "d")
        or sfx("ún", "una", "euscald")
        or sfx("an", "á", "ch")
        or sfx("que", "ca", "turule")
        or sfx("cónsul", "consulesa")
        or sfx("conde", "condesa")
        or sfx("margrave", "margravina")
        or sfx("un", "unha")
        or sfx("ún", "unha")
        or sfx("eu", "úa", "[st]")
        or sfx("dous", "dúas")
    ):
        return 17
    raise NotImplementedError(
        f"Falta o código para determinar a regra de formación do feminino "
        f"para o masculino «{masculine}» e o feminino «{feminine}»."
    )


class Status(Enum):
    RECOMMENDED = 1
    ALLOWED = 2


class Entry:
    def __init__(
        self,
        lemma,
        *,
        feminine_lemmas,
        pos_feminine_lemmas=None,
        invariable=False,
        parts_of_speech=None,
        nested_parts_of_speech=None,
        same_as=None,
        better_forms=None,
        singular=None,
        conjugation=None,
        auxiliar=None,
    ):
        self.lemma = lemma
        self.singular = singular
        self.conjugation = conjugation
        self.auxiliar = auxiliar
        self.feminine_lemmas = set(feminine_lemmas)
        self.pos_feminine_lemmas = {
            k: set(v) for k, v in list((pos_feminine_lemmas or {}).items())
        }
        self.invariable = invariable
        self.parts_of_speech = set(parts_of_speech) if parts_of_speech else set()
        self.nested_parts_of_speech = {
            k: set(v) for k, v in list((nested_parts_of_speech or {}).items())
        }
        self.same_as = same_as
        self.better_forms = better_forms
        if better_forms:
            self.status = Status.ALLOWED
        else:
            self.status = Status.RECOMMENDED

    def json(self):
        return json.dumps(
            {
                "lemma": self.lemma,
                "feminine_lemmas": sorted(self.feminine_lemmas),
                "pos_feminine_lemmas": {
                    k: sorted(v) for k, v in list(self.pos_feminine_lemmas.items())
                },
                "invariable": self.invariable,
                "parts_of_speech": sorted(self.parts_of_speech),
                "nested_parts_of_speech": {
                    k: sorted(v) for k, v in list(self.nested_parts_of_speech.items())
                },
                "same_as": self.same_as,
                "better_forms": self.better_forms,
                "singular": self.singular,
                "conjugation": self.conjugation,
                "auxiliar": self.auxiliar,
            },
            ensure_ascii=False,
        )


def pluralize(singular):
    if P_12_RE.search(singular):
        return f"{singular[:-1]}is"
    if P_NONE_RE.search(singular) or singular[0].isupper():
        return singular
    if singular.endswith("r"):
        return f"{singular}es"
    return f"{singular}s"


ACUTE_MAP = {
    "a": "á",
    "e": "é",
    "i": "í",
    "o": "ó",
    "u": "ú",
}


def switch_flag_ar(key_form, fallback=None):
    if key_form is None:
        assert isinstance(fallback, str)
        return bool(re.search(r"[^aeo]iar$", fallback))
    return bool(re.search(r"[aeio][íú]|[^qg]u[íú]|^u[íú]|[íú][aeo]", key_form))


def find_u_o_and_e_i(infinitive, i_p_p1, i_p_p3):
    u_o = e_i1 = e_i2 = False
    alt_infinitive = infinitive
    if i_p_p3 and i_p_p3 != "-":
        alt_infinitive = i_p_p3[:-1] + "ir"
        if infinitive != alt_infinitive:
            diff_char_idx = next(
                (
                    i
                    for i, (char1, char2) in enumerate(zip(infinitive, alt_infinitive))
                    if char1 != char2
                ),
                None,
            )
            if infinitive[diff_char_idx] == "u" and (
                infinitive[diff_char_idx - 1] not in "qg"
                or infinitive[diff_char_idx + 1] not in "ei"
            ):
                u_o = True
            elif infinitive[diff_char_idx] == "e":
                e_i1 = True
    if not u_o and not e_i1 and i_p_p1 and i_p_p1 != "-":
        if i_p_p1[-2:] == "go":
            alt_infinitive = i_p_p1[:-1] + "uir"
        else:
            alt_infinitive = i_p_p1[:-1] + "ir"
        if infinitive != alt_infinitive:
            diff_char_idx = next(
                (
                    i
                    for i, (char1, char2) in enumerate(zip(infinitive, alt_infinitive))
                    if char1 != char2
                ),
                None,
            )
            if infinitive[diff_char_idx] == "u" and (
                infinitive[diff_char_idx - 1] not in "qg"
                or infinitive[diff_char_idx + 1] not in "ei"
            ):
                u_o = True
            elif infinitive[diff_char_idx] == "e":
                e_i2 = True
    return u_o, e_i1, e_i2, alt_infinitive


def get_allomorphs(infinitive, conjugation):
    i_p_p3_raw = conjugation.get("i_p_p3")
    if (
        infinitive.endswith("ír")
        or infinitive.endswith("por")
        or (
            infinitive.endswith("ir")
            and conjugation.get("i_p_p4") != "-"
            and conjugation.get("s_p_p4") == "-"
        )
    ):
        return
    if infinitive.endswith("facer"):
        prefix = infinitive[: -len("facer")]
        yield f"{prefix}fager"
        yield f"{prefix}fáger"
        yield f"{prefix}far"
        yield f"{prefix}fixer"
        yield f"{prefix}fíxer"
        return
    if infinitive.endswith("poñer"):
        prefix = infinitive[: -len("poñer")]
        yield f"{prefix}póñer"
        yield f"{prefix}puxer"
        yield f"{prefix}púxer"
        return
    i_p_p1 = conjugation.get("i_p_p1")
    if infinitive.endswith("ter") and i_p_p1 and i_p_p1.endswith("eño"):
        prefix = infinitive[: -len("ter")]
        yield f"{prefix}teñer"
        yield f"{prefix}téñer"
        yield f"{prefix}tiñer"
        yield f"{prefix}tiver"
        yield f"{prefix}tíver"
        return
    if infinitive.endswith("dicir"):
        prefix = infinitive[: -len("dicir")]
        yield f"{prefix}dixer"
        return
    if infinitive.endswith("vir") and i_p_p1 and i_p_p1.endswith("eño"):
        prefix = infinitive[: -len("vir")]
        yield f"{prefix}viñer"
        return
    if i_p_p3_raw is None:
        yield get_allomorph(infinitive, None)
        return
    if isinstance(i_p_p3_raw, dict):
        seen = set()
        for i_p_p3 in set(i_p_p3_raw.values()):
            allomorph = get_allomorph(infinitive, i_p_p3)
            if allomorph in seen:
                continue
            seen.add(allomorph)
            yield allomorph
        return
    for i_p_p3 in i_p_p3_raw.split("/"):
        if infinitive.endswith("ir"):
            u_o, e_i1, e_i2, alt_infinitive = find_u_o_and_e_i(
                infinitive, conjugation.get("i_p_p1"), i_p_p3
            )
            if u_o or e_i2:
                yield get_allomorph(infinitive, i_p_p3)
                yield alt_infinitive
                yield get_allomorph(alt_infinitive, i_p_p3)
                return
            if e_i1:
                yield alt_infinitive
                yield get_allomorph(alt_infinitive, i_p_p3)
                return
        yield get_allomorph(infinitive, i_p_p3)


def get_allomorph(infinitive, i_p_p3):
    if i_p_p3 and any(acute in i_p_p3 for acute in ACUTE_MAP.values()):
        return i_p_p3[:-1] + infinitive[-2:]

    prefix = infinitive[:-2]  # Retirar «-ar», «-er», «-ir» ou «-or».
    if infinitive[-2] in "ae" and len(infinitive) >= 3 and infinitive[-3] in "iu":
        prefix = prefix[:-1]
    prefix = re.sub(rf"(?:{CONSONANTE}|[qg]u$)*$", "", prefix)  # Retirar consonantes.
    if not prefix:
        logger.warning(
            f"Non foi posíbel determinar o alomorfo de «{infinitive}», o "
            f"prefixo quedou baleiro tras retirar consonantes."
        )
        return infinitive
    if not switch_flag_ar(i_p_p3, infinitive) and (
        (
            prefix[-1] in "i"
            and len(prefix) >= 2
            and (
                prefix[-2] in "aeo"
                or (prefix[-2] == "u" and len(prefix) >= 3 and prefix[-3] not in "qg")
            )
        )
        or (prefix[-1] in "u" and len(prefix) >= 2 and prefix[-2] in "aeoi")
    ):
        prefix = prefix[:-1]
    len(prefix) - 1
    try:
        acuted = ACUTE_MAP[prefix[-1]]
    except KeyError:
        logger.warning(
            f"Non foi posíbel determinar o alomorfo de «{infinitive}», o "
            f"carácter que se ía acentuar, «{prefix[-1]}», non é unha vogal "
            f"sen til."
        )
        return infinitive
    return prefix[:-1] + acuted + infinitive[len(prefix) :]


def conjugate(time, mode, conjugation):
    if not conjugation:
        return None
    if isinstance(conjugation[time], dict):
        return conjugation[time].get(f"verbo {mode}", None)
    return conjugation[time]


def get_verb_flags(infinitive, modes, conjugation):
    flags = []

    if infinitive.endswith("facer"):
        if "intransitivo" in modes:
            flags.extend((420, 421, 432, 428, 429))
        if "pronominal" in modes:
            flags.extend((520, 521, 532, 528, 529))
        if "transitivo" in modes:
            flags.extend((320, 321, 332, 328, 329))
        return tuple(flags)
    if infinitive.endswith("poñer"):
        if "intransitivo" in modes:
            flags.extend((420, 421, 432, 426, 427, 428, 429))
        if "pronominal" in modes:
            flags.extend((520, 521, 532, 526, 527, 528, 529))
        if "transitivo" in modes:
            flags.extend((320, 321, 332, 326, 327, 328, 329))
        return tuple(flags)

    i_p_p1 = conjugation.get("i_p_p1")
    if infinitive.endswith("ter") and i_p_p1 and i_p_p1.endswith("eño"):
        if "intransitivo" in modes:
            if infinitive == "ter":
                flags.extend((420, 421, 422, 427, 429))
            else:
                flags.extend((420, 421, 427, 429, 432))
        if "pronominal" in modes:
            if infinitive == "ter":
                flags.extend((520, 521, 522, 527, 529))
            else:
                flags.extend((520, 521, 527, 529, 532))
        if "transitivo" in modes:
            if infinitive == "ter":
                flags.extend((320, 321, 322, 327, 329))
            else:
                flags.extend((320, 321, 327, 329, 332))
        return tuple(flags)
    if infinitive.endswith("dicir"):
        if "intransitivo" in modes:
            if infinitive == "dicir":
                flags.extend((734, 736, 737, 738))
            else:
                flags.extend((735, 736, 737, 738))
        if "pronominal" in modes:
            if infinitive == "dicir":
                flags.extend((834, 836, 837, 838))
            else:
                flags.extend((835, 836, 837, 838))
        if "transitivo" in modes:
            if infinitive == "dicir":
                flags.extend((634, 636, 637, 638))
            else:
                flags.extend((635, 636, 637, 638))
        return tuple(flags)
    if infinitive.endswith("vir") and i_p_p1 and i_p_p1.endswith("eño"):
        if "intransitivo" in modes:
            flags.extend((740, 741, 745))
        if "pronominal" in modes:
            flags.extend((840, 841, 845))
        if "transitivo" in modes:
            flags.extend((640, 641, 645))
        return tuple(flags)
    if infinitive.endswith("por"):
        if "intransitivo" in modes:
            flags.extend((450, 451))
        if "pronominal" in modes:
            flags.extend((550, 551))
        if "transitivo" in modes:
            flags.extend((350, 351))
        return tuple(flags)

    def add_zero(conjugated):
        return (
            not switch_flag_ar(conjugated, infinitive)
            or conjugated
            and "/" in conjugated
        )

    if infinitive[-2:] == "ar":

        def handle_mode(mode, flag136, flag36):
            if mode in modes:
                _add_zero = add_zero(conjugate("i_p_p3", mode, conjugation))
                if conjugate("i_p_p1", mode, conjugation) == "-":
                    if _add_zero:
                        flags.append(flag36)
                    flags.append(flag36 + 1)
                else:
                    if _add_zero:
                        flags.append(flag136)
                    flags.append(flag136 + 1)

        handle_mode("intransitivo", 220, 270)
        handle_mode("pronominal", 230, 280)
        handle_mode("transitivo", 200, 250)

    elif infinitive[-2:] == "er":

        def handle_mode(mode, flag136, flag36, flag3):
            if mode in modes:
                if conjugate("i_p_p1", mode, conjugation) == "-":
                    if conjugate("i_p_p6", mode, conjugation) == "-":
                        flags.append(flag3)
                    else:
                        flags.append(flag36)
                else:
                    flags.append(flag136)
                    flags.append(flag136 + 1)
                    flags.append(flag136 + 2)

        handle_mode("intransitivo", 410, 460, 470)
        handle_mode("pronominal", 510, 560, 570)
        handle_mode("transitivo", 310, 360, 370)

    elif infinitive[-2:] in ("ir", "ír"):

        def handle_mode(mode, flag136, flag36):
            if mode in modes:
                i_p_p1 = conjugate("i_p_p1", mode, conjugation)
                if not i_p_p1:
                    raise ValueError(f"O verbo {infinitive} non ten conxugación.")
                i_p_p3 = conjugate("i_p_p3", mode, conjugation)
                i_p_p4 = conjugate("i_p_p4", mode, conjugation)
                s_p_p4 = conjugate("s_p_p4", mode, conjugation)
                _add_zero = add_zero(i_p_p3)
                u_o, e_i1, e_i2, _ = find_u_o_and_e_i(infinitive, i_p_p1, i_p_p3)
                if i_p_p1 == "-" and i_p_p4 == "-":
                    flags.append(flag36)
                elif s_p_p4 == "-":
                    flags.append(flag36 + 8)
                    flags.append(flag36 + 9)
                else:
                    if _add_zero and not infinitive.endswith("oír"):
                        flags.append(flag136)
                    if infinitive.endswith("ír"):
                        flags.append(flag136 + 13)
                    elif infinitive.endswith("vir") and i_p_p1.endswith("zo"):
                        flags.append(flag136 + 6)
                    elif infinitive.endswith("parir"):
                        flags.append(flag136 + 8)
                    elif u_o:
                        flags.append(flag136 + 14)
                    elif e_i1:
                        flags.append(flag136 + 20)
                    elif e_i2:
                        flags.append(flag136 + 20)
                        flags.append(flag136 + 21)
                    else:
                        flags.append(flag136 + 1)

        handle_mode("intransitivo", 700, 750)
        handle_mode("pronominal", 800, 850)
        handle_mode("transitivo", 600, 650)

    return tuple(flags)


def get_allomorph_flags(infinitive, allomorph, modes, conjugation):
    flags = []

    if infinitive.endswith("facer"):
        if allomorph.endswith("fager"):
            if "intransitivo" in modes:
                flags.append(426)
            if "pronominal" in modes:
                flags.append(526)
            if "transitivo" in modes:
                flags.append(326)
            return tuple(flags)
        if allomorph.endswith("fáger"):
            if "intransitivo" in modes:
                flags.append(442)
            if "pronominal" in modes:
                flags.append(542)
            if "transitivo" in modes:
                flags.append(342)
            return tuple(flags)
        if allomorph.endswith("far"):
            if "intransitivo" in modes:
                flags.append(427)
            if "pronominal" in modes:
                flags.append(527)
            if "transitivo" in modes:
                flags.append(327)
            return tuple(flags)
        if allomorph.endswith("fixer"):
            if "intransitivo" in modes:
                flags.extend((423, 424, 425))
            if "pronominal" in modes:
                flags.extend((523, 524, 525))
            if "transitivo" in modes:
                flags.extend((323, 324, 325))
            return tuple(flags)
        if allomorph.endswith("fíxer"):
            if "intransitivo" in modes:
                flags.append(441)
            if "pronominal" in modes:
                flags.append(541)
            if "transitivo" in modes:
                flags.append(341)
            return tuple(flags)
        assert False

    if infinitive.endswith("poñer"):
        if allomorph.endswith("póñer"):
            if "intransitivo" in modes:
                flags.append(442)
            if "pronominal" in modes:
                flags.append(542)
            if "transitivo" in modes:
                flags.append(342)
            return tuple(flags)
        if allomorph.endswith("puxer"):
            if "intransitivo" in modes:
                flags.extend((423, 424, 425))
            if "pronominal" in modes:
                flags.extend((523, 524, 525))
            if "transitivo" in modes:
                flags.extend((323, 324, 325))
            return tuple(flags)
        if allomorph.endswith("púxer"):
            if "intransitivo" in modes:
                flags.append(441)
            if "pronominal" in modes:
                flags.append(541)
            if "transitivo" in modes:
                flags.append(341)
            return tuple(flags)
        assert False

    i_p_p1 = conjugation.get("i_p_p1")
    if infinitive.endswith("ter") and i_p_p1 and i_p_p1.endswith("eño"):
        if allomorph.endswith("teñer"):
            if "intransitivo" in modes:
                flags.append(426)
            if "pronominal" in modes:
                flags.append(526)
            if "transitivo" in modes:
                flags.append(326)
            return tuple(flags)
        if allomorph.endswith("téñer"):
            if "intransitivo" in modes:
                flags.append(442)
            if "pronominal" in modes:
                flags.append(542)
            if "transitivo" in modes:
                flags.append(342)
            return tuple(flags)
        if allomorph.endswith("tiñer"):
            if "intransitivo" in modes:
                flags.append(430)
            if "pronominal" in modes:
                flags.append(530)
            if "transitivo" in modes:
                flags.append(330)
            return tuple(flags)
        if allomorph.endswith("tiver"):
            if "intransitivo" in modes:
                flags.extend((423, 424, 425))
            if "pronominal" in modes:
                flags.extend((523, 524, 525))
            if "transitivo" in modes:
                flags.extend((323, 324, 325))
            return tuple(flags)
        if allomorph.endswith("tíver"):
            if "intransitivo" in modes:
                flags.append(441)
            if "pronominal" in modes:
                flags.append(541)
            if "transitivo" in modes:
                flags.append(341)
            return tuple(flags)
        assert False
    if infinitive.endswith("dicir"):
        if "intransitivo" in modes:
            flags.extend((738, 739))
        if "pronominal" in modes:
            flags.extend((838, 839))
        if "transitivo" in modes:
            flags.extend((638, 639))
        return tuple(flags)
    if infinitive.endswith("vir") and i_p_p1 and i_p_p1.endswith("eño"):
        if "intransitivo" in modes:
            flags.extend((741, 743, 744))
        if "pronominal" in modes:
            flags.extend((841, 843, 844))
        if "transitivo" in modes:
            flags.extend((641, 643, 644))
        return tuple(flags)

    add_zero = switch_flag_ar(allomorph)

    if allomorph[-2:] == "ar":

        def handle_mode(mode, flag136, flag36):
            if mode in modes:
                if conjugate("i_p_p1", mode, conjugation) == "-":
                    if add_zero:
                        flags.append(flag36)
                    flags.append(flag36 + 2)
                else:
                    if add_zero:
                        flags.append(flag136)
                    flags.append(flag136 + 2)

        handle_mode("intransitivo", 220, 270)
        handle_mode("pronominal", 230, 280)
        handle_mode("transitivo", 200, 250)

    elif allomorph[-2:] == "er":

        def handle_mode(mode, flag136, flag36, flag3):
            if mode in modes:
                if conjugate("i_p_p1", mode, conjugation) == "-":
                    if conjugate("i_p_p6", mode, conjugation) == "-":
                        flags.append(flag3 + 1)
                    else:
                        flags.append(flag36 + 1)
                else:
                    flags.append(flag136 + 5)

        handle_mode("intransitivo", 410, 460, 470)
        handle_mode("pronominal", 510, 560, 570)
        handle_mode("transitivo", 310, 360, 370)

    elif allomorph[-2:] == "ir":

        def handle_mode(mode, flag136, flag36):
            if mode in modes:
                i_p_p1 = conjugate("i_p_p1", mode, conjugation)
                i_p_p3 = conjugate("i_p_p3", mode, conjugation)
                i_p_p4 = conjugate("i_p_p4", mode, conjugation)
                s_p_p4 = conjugate("s_p_p4", mode, conjugation)
                if i_p_p1 == "-" and i_p_p4 == "-":
                    if i_p_p3 == "-" and s_p_p4 == "-":
                        pass
                    elif "í" in allomorph:
                        flags.append(flag36 + 3)
                        flags.append(flag36 + 5)
                    else:
                        flags.append(flag36 + 3)
                        flags.append(flag36 + 4)
                else:
                    u_o, e_i1, e_i2, _ = find_u_o_and_e_i(
                        infinitive, i_p_p1, conjugate("i_p_p3", mode, conjugation)
                    )
                    if add_zero:
                        flags.append(flag136)
                    if infinitive.endswith("vir") and i_p_p1.endswith("zo"):
                        flags.append(flag136 + 7)
                    elif infinitive.endswith("parir"):
                        flags.append(flag136 + 9)
                    elif u_o:
                        if "ó" in allomorph:
                            flags.append(flag136 + 17)
                        elif "ú" in allomorph:
                            flags.append(flag136 + 15)
                        else:
                            flags.append(flag136 + 16)
                    elif e_i1:
                        if "í" in allomorph:
                            flags.append(flag136 + 22)
                            flags.append(flag136 + 24)
                        else:
                            flags.append(flag136 + 21)
                            flags.append(flag136 + 23)
                    elif e_i2:
                        if "í" in allomorph:
                            flags.append(flag136 + 24)
                        elif "é" in allomorph:
                            flags.append(flag136 + 22)
                        else:
                            flags.append(flag136 + 23)
                    else:
                        flags.append(flag136 + 2)

        handle_mode("intransitivo", 700, 750)
        handle_mode("pronominal", 800, 850)
        handle_mode("transitivo", 600, 650)

    return tuple(flags)


ALL_MODES = ("transitivo", "intransitivo", "pronominal")


DISMISSABLE_WORDS = "|".join(
    re.escape(word)
    for word in (
        "a",
        "ao",
        "aos",
        "á",
        "ás",
        "con",
        "co",
        "cos",
        "coa",
        "coas",
        "cun",
        "cuns",
        "cunha",
        "cunhas",
        "de",
        "do",
        "dos",
        "da",
        "das",
        "dun",
        "duns",
        "dunha",
        "dunhas",
        "en",
        "no",
        "nos",
        "na",
        "nas",
        "nun",
        "nuns",
        "nunha",
        "nunhas",
        "por",
        "polo",
        "polos",
        "pola",
        "polas",
    )
)


def _is_dismissable_ngram_part(part):
    return re.search(
        rf"^(?:{DISMISSABLE_WORDS}|[\d,’‘]+)$",
        part,
    )


class Builder:
    def __init__(self, output_path):
        self._build_dir = TemporaryDirectory()
        self._output_path = output_path
        self._tmp_output_path = Path(self._build_dir.name) / "a"
        self._tmp_output_file = self._tmp_output_path.open("w")

    def _add_entry(self, entry, pos):
        lemma = entry.singular or entry.lemma
        if DEBUG_WORD == lemma:
            caller = getframeinfo(stack()[1][0])
            logger.debug(
                f"Builder._add_entry(Entry<{lemma}>, {pos!r}) [called from "
                f"L{caller.lineno}]"
            )
        other_pos = entry.parts_of_speech - pos
        p_rule = get_plural_rule(lemma)
        p_rules = (p_rule,) if p_rule else tuple()
        invariable = entry.invariable if isinstance(entry.invariable, dict) else {}
        if all(
            (
                po in INFLEXIBLE_POS
                or invariable.get(po)
                or po == "numeral"
                and INFLEX_NUM_RE.search(entry.lemma)
            )
            for po in pos
        ):
            pos |= {
                po
                for po in other_pos
                if (
                    po in INFLEXIBLE_POS
                    or invariable.get(po)
                    or po == "numeral"
                    and INFLEX_NUM_RE.search(entry.lemma)
                )
            }
            if all("plural" in po for po in pos) and not all(
                "plural" in po for po in other_pos
            ):
                lemma = pluralize(lemma)
            self._add(lemma, po=pos)
            entry.parts_of_speech -= pos
            return

        feminine_lemmas = entry.feminine_lemmas
        if entry.pos_feminine_lemmas:
            assert len(entry.pos_feminine_lemmas) == 1
            assert set(entry.pos_feminine_lemmas) == pos
            po = next(iter(entry.pos_feminine_lemmas))
            feminine_lemmas = entry.pos_feminine_lemmas[po]

        if all(po in FEM_POS for po in pos):
            pos |= {po for po in other_pos if po in FEM_POS}
            if feminine_lemmas:
                for lemma in feminine_lemmas:
                    self._add(lemma, p_rules, pos)
            else:
                self._add(lemma, p_rules, pos)
            entry.parts_of_speech -= pos
            return

        if all(po in MASC_POS for po in pos):
            pos |= {po for po in other_pos if po in MASC_POS}
            self._add(lemma, p_rules, pos)
            entry.parts_of_speech -= pos
            return

        if not feminine_lemmas:
            if other_pos <= NO_NUMBER_POS:
                self._add(lemma, p_rules, pos)
                entry.parts_of_speech -= pos
            return

        if other_pos <= NO_GENDER_OR_NUMBER_POS:
            assert len(feminine_lemmas) == 1
            masculine = lemma
            feminine = next(iter(feminine_lemmas))
            try:
                f_rule = get_feminine_rule(masculine, feminine)
            except NotImplementedError:
                self._add(masculine, p_rules, {f"{po} masculino" for po in pos})
                self._add(feminine, p_rules, {f"{po} feminino" for po in pos})
            else:
                self._add(lemma, p_rules + (f_rule,), pos)
            entry.parts_of_speech -= pos
            return

    def _add(self, word, flags=tuple(), po=None):
        if word == DEBUG_WORD:
            caller = getframeinfo(stack()[1][0])
            logger.debug(
                f"Builder.add({word!r}, {flags!r}, {po!r}) [called from "
                f"L{caller.lineno}]"
            )
        word = word.rstrip(".")
        if "." in word:
            return
        if isinstance(flags, int):
            flags = (flags,)
        if word not in (word.lower(), word.capitalize()):
            flags = (*flags, 999)
        if flags:
            flag_string = "/" + ",".join(str(flag) for flag in sorted(flags))
        else:
            flag_string = ""
        if po is None:
            po_set = set()
        else:
            if isinstance(po, str):
                po = (po,)
            po_set = set(po)
        self._pos_being_added |= po_set
        po_set = tuple(sorted(po_set))
        po_string = "".join(f' po:{_po.replace(" ", "_")}' for _po in po_set)
        if "-" in word or " " in word:
            line = ""
            all_parts = re.split(r"[\s-]", word)
            ngram = f" is:ngrama_{'_'.join(all_parts)}"
            parts = [part for part in all_parts if not _is_dismissable_ngram_part(part)]
            try:
                de_index = all_parts.index("de")
            except ValueError:
                de_index = None
            if de_index is not None:
                for index, part in enumerate(all_parts):
                    if part not in parts or part in all_parts[:index]:
                        continue
                    if index < de_index:
                        line += f"{part}{flag_string}{po_string}{ngram}\\n"
                    else:
                        line += f"{part}{po_string}{ngram}\\n"
                if line:
                    line = f"{line[:-2]}\n"
            elif "monoespazo" in parts:
                for index, part in enumerate(all_parts):
                    if part not in parts or part in all_parts[:index]:
                        continue
                    if part != "monoespazo":
                        line += f"{part}{flag_string}{po_string}{ngram}\\n"
                    else:
                        line += f"{part}{po_string}{ngram}\\n"
                if line:
                    line = f"{line[:-2]}\n"
            elif parts:
                for index, part in enumerate(all_parts):
                    if (
                        part not in parts
                        or part in all_parts[:index]
                        and (not flags or part != all_parts[-1])
                    ):
                        continue
                    if index == len(all_parts) - 1 and part == all_parts[-1]:
                        line += f"{part}{flag_string}{po_string}{ngram}\\n"
                    else:
                        line += f"{part}{po_string}{ngram}\\n"
                if line:
                    line = f"{line[:-2]}\n"
        else:
            line = f"{word}{flag_string}{po_string}\n"
        self.write_line(line)

    def write_line(self, line):
        self._tmp_output_file.write(line)

    def _add_verb(self, infinitive, modes, conjugation, auxiliar):
        conjugation = conjugation or {}
        if infinitive == DEBUG_WORD:
            caller = getframeinfo(stack()[1][0])
            logger.debug(
                f"Builder._add_verb({infinitive!r}, {modes!r}, "
                f"{conjugation!r}, {auxiliar!r}) [called from "
                f"L{caller.lineno}]"
            )

        if infinitive in ("advir", "convir", "desconvir", "intervir", "sobrevir"):
            line = (
                f"{infinitive}/740,741,745 "
                f"po:verbo ts:intransitiva al:{infinitive[:-1]}ñer\\n"
                f"{infinitive[:-1]}ñer/666,741,743,744 st:{infinitive}\n"
            )
        elif infinitive == "bendicir":
            line = (
                "bendicir/600,601,635,636 po:verbo ts:transitiva al:bendícir\\n"
                "bendícir/666,602 st:bendicir\\n"
                "bendixer/666,638,639 st:bendicir\n"
            )
        elif infinitive == "caber":
            line = (
                "caber/420,421,422,427,428,429 "
                "po:verbo ts:intransitiva "
                "al:cáber al:caiber al:cáiber al:couber al:cóuber\\n"
                "cáber/666,440 st:caber\\n"
                "caiber/666,426 st:caber\\n"
                "cáiber/666,442 st:caber\\n"
                "couber/666,423,424,425 st:caber\\n"
                "cóuber/666,441 st:caber\n"
            )
        elif infinitive in ("crer", "ler"):
            line = (
                f"{infinitive}/310,312,313,410,412,413 "
                "po:verbo ts:transitiva ts:intransitiva\n"
            )
        elif infinitive == "cumprir":
            line = (
                "cumprir/600,601,700,701,800,801 "
                "po:verbo ts:transitiva ts:intransitiva ts:pronominal "
                "al:cómprir al:cúmprir\\n"
                "cómprir/666,753 st:cumprir\\n"
                "cúmprir/666,602,702,802 st:cumprir\n"
            )
        elif infinitive == "dar":
            line = (
                "dar/205,225,235 po:verbo ts:transitiva ts:intransitiva "
                "ts:pronominal\n"
            )
        elif infinitive == "descrer":
            line = (
                "descrer/310,312,314,410,412,414 "
                "po:verbo ts:transitiva ts:intransitiva\n"
            )
        elif infinitive == "desprover":
            line = (
                "desprover/311,320,321,323,327,328,329,332 "
                "po:verbo ts:transitiva al:desprovexer al:desprovéxer\\n"
                "desprovexer/666,326 st:desprover\\n"
                "desprovéxer/666,342 st:desprover\n"
            )
        elif infinitive == "entrever":
            line = (
                "entrever/320,321,327,328,329,332 "
                "po:verbo ts:transitiva al:entrevexer "
                "al:entrevéxer al:entrevir\\n"
                "entrevexer/666,326 st:entrever\\n"
                "entrevéxer/666,342 st:entrever\\n"
                "entrevir/666,323,324,335 st:entrever\n"
            )
        elif infinitive == "equivaler":
            line = (
                "equivaler/420,421,422,424,425,427,428,429 "
                "po:verbo ts:intransitiva al:equiváler al:equivaller "
                "al:equiváller\\n"
                "equiváler/666,440,441 st:equivaler\\n"
                "equivaller/666,426 st:equivaler\\n"
                "equiváller/666,442 st:equivaler\n"
            )
        elif infinitive == "estar":
            line = "estar/205,225,235 po:verbo ts:intransitiva ts:pronominal\n"
        elif infinitive == "facer":
            line = (
                "facer/320,321,322,328,329,420,421,422,428,429,520,521,522,528,529 "
                "po:verbo ts:transitiva ts:intransitiva ts:pronominal "
                "al:fácer al:fager al:fáger al:far al:fixer al:fíxer\\n"
                "fácer/666,340,440,540 st:facer\\n"
                "fager/666,326,426,526 st:facer\\n"
                "fáger/666,342,442,542 st:facer\\n"
                "far/666,327,427,527 st:facer\\n"
                "fixer/666,323,324,325,423,424,425,523,524,525 st:facer\\n"
                "fíxer/666,341,441,541 st:facer\n"
            )
        elif infinitive == "fundir":
            line = (
                "fundir/600,601,614,800,801,814 "
                "po:verbo ts:transitiva ts:pronominal al:fondir al:fúndir\\n"
                "fondir/666,753 st:fundir\\n"
                "fúndir/666,602,615,802,815 st:fundir\n"
            )
        elif infinitive == "haber":
            line = (
                "haber/320,321,322,327,328,329,420,421,422,427,428,429,520,"
                "521,522,527,528,529 "
                "po:verbo ts:transitiva ts:auxiliar "
                "al:háber al:haxer al:háxer al:houber al:hóuber\\n"
                "háber/666,340,440,540 st:haber\\n"
                "haxer/666,326,426,526 st:haber\\n"
                "háxer/666,342,442,542 st:haber\\n"
                "houber/666,323,324,325,423,424,425,523,524,525 st:haber\\n"
                "hóuber/666,341,441,541 st:haber\n"
            )
        elif infinitive == "incumbir":
            line = (
                "incumbir/750,751 po:verbo ts:defectiva_intransitiva al:incúmbir\\n"
                "incúmbir/666,752 st:incumbir\n"
            )
        elif infinitive == "ir":
            line = (
                "ir/630,631,730,731,830,831 "
                "po:verbo ts:intransitiva ts:auxiliar al:for al:ír al:vair\\n"
                "for/666,631,632,731,732,831,832 st:ir\\n"
                "ír/630,730,830 st:ir\\n"
                "vair/666,633,733,833 st:ir\n"
            )
        elif infinitive == "maldicir":
            line = (
                "maldicir/600,601,635,636,700,701,735,736 "
                "po:verbo ts:transitiva ts:intransitiva al:maldixer\\n"
                "maldícir/666,602,702 st:maldicir\\n"
                "maldixer/666,638,639,738,739 st:maldicir\n"
            )
        elif infinitive == "oír":
            line = "oír/613 po:verbo ts:transitiva al:óir\\n" "óir/666,613 st:oír\n"
        elif infinitive == "poder":
            line = (
                "poder/320,321,322,327,328,329,420,421,422,427,428,429,520,"
                "521,522,527,528,529 "
                "po:verbo ts:transitiva ts:intransitiva ts:auxiliar "
                "al:póder al:poider al:póider al:puider al:púider\\n"
                "póder/666,340,440,540 st:poder\\n"
                "poider/666,326,426,526 st:poder\\n"
                "póider/666,342,442,542 st:poder\\n"
                "puider/666,323,324,325,423,424,425,523,524,525 st:poder\\n"
                "púider/666,341,441,541 st:poder\n"
            )
        elif infinitive == "poñer":
            line = (
                "poñer/320,321,322,326,327,328,329,420,421,422,426,427,428,"
                "429,520,521,522,526,527,528,529 "
                "po:verbo ts:transitiva ts:pronominal al:póñer al:puxer al:púxer\\n"
                "póñer/666,340,342,440,442,540,542 st:poñer\\n"
                "puxer/666,323,324,325,423,424,425,523,524,525 st:poñer\\n"
                "púxer/666,341,441,541 st:poñer\n"
            )
        elif infinitive == "pór":
            line = (
                "pór/350,450,550 "
                "po:verbo ts:transitiva ts:pronominal ts:auxiliar al:por\\n"
                "por/666,351,451,551 st:pór\n"
            )
        elif infinitive == "pracer":
            line = (
                "pracer/464 "
                "po:verbo ts:defectiva_intransitiva "
                "al:prácer al:próuger al:prouguer\\n"
                "prácer/666,466 st:pracer\\n"
                "próuger/666,467 st:pracer\\n"
                "prouguer/666,465 st:pracer\n"
            )
        elif infinitive == "prever":
            line = (
                "prever/320,321,327,328,329,332 po:verbo ts:transitiva "
                "al:prevexer al:prevéxer al:previr\\n"
                "prevexer/666,326 st:prever\\n"
                "prevéxer/666,342 st:prever\\n"
                "previr/666,323,324,335 st:prever\n"
            )
        elif infinitive == "prover":
            line = (
                "prover/311,320,321,323,327,328,329,332,511,520,521,523,527,"
                "528,529,532 "
                "po:verbo ts:transitiva ts:pronominal al:provexer al:provéxer\\n"
                "provexer/666,326,526 st:prover\\n"
                "provéxer/666,342,542 st:prover\n"
            )
        elif infinitive == "querer":
            line = (
                "querer/320,321,322,327,328,329,420,421,422,427,428,429 "
                "po:verbo ts:transitiva ts:intransitiva "
                "al:queirer al:quéirer al:quérer al:quixer al:quíxer\\n"
                "queirer/666,326,426 st:querer\\n"
                "quéirer/666,342,442 st:querer\\n"
                "quérer/666,340,440 st:querer\\n"
                "quixer/666,323,324,325,423,424,425 st:querer\\n"
                "quíxer/666,341,441 st:querer\n"
            )
        elif infinitive == "reler":
            line = "reler/310,312,314 po:verbo ts:transitiva\n"
        elif infinitive == "reunir":
            line = "reunir/600,605,800,805 po:verbo ts:transitiva ts:pronominal\n"
        elif infinitive == "rever":
            line = (
                "rever/320,321,327,328,329,332,420,421,427,429,432,438 "
                "po:verbo ts:transitiva ts:intransitiva al:revexer "
                "al:revéxer al:revir\\n"
                "revexer/666,326,426 st:rever\\n"
                "revéxer/666,342,442 st:rever\\n"
                "revir/666,323,324,335,423,424,435 st:rever\n"
            )
        elif infinitive == "rir":
            line = (
                "rir/600,603,700,703,800,803 "
                "po:verbo ts:transitiva ts:intransitiva ts:pronominal\n"
            )
        elif infinitive == "saber":
            line = (
                "saber/320,321,322,327,328,329,420,421,422,427,428,429 "
                "po:verbo ts:transitiva ts:intransitiva "
                "al:sáber al:saiber al:sáiber al:souber al:sóuber\\n"
                "sáber/666,340,440 st:saber\\n"
                "saiber/666,326,426 st:saber\\n"
                "sáiber/666,342,442 st:saber\\n"
                "souber/666,323,324,325,423,424,425 st:saber\\n"
                "sóuber/666,341,441 st:saber\n"
            )
        elif infinitive in ("saír", "sobresaír"):
            line = f"{infinitive}/713 po:verbo ts:intransitiva\n"
        elif infinitive == "ser":
            line = (
                "ser/320,321,322,326,327,329,420,421,422,426,427,429,520,521,"
                "522,526,527,529 "
                "po:verbo ts:intransitiva ts:copulativa ts:auxiliar "
                "al:erer al:érer al:eser al:for al:sexer\\n"
                "érer/666,322,330,422,430,522,530 st:ser\\n"
                "erer/666,330,430,530 st:ser\\n"
                "eser/666,322,422,522 st:ser\\n"
                "for/666,323,324,325,423,424,425,523,524,525 st:ser\\n"
                "sexer/666,326,426,526 st:ser\n"
            )
        elif infinitive == "sobreser":
            line = "sobreser/310,312,314 po:verbo ts:transitiva\n"
        elif infinitive == "sorrir":
            line = "sorrir/700,704 po:verbo ts:intransitiva\n"
        elif infinitive == "traer":
            line = (
                "traer/320,321,322,326,327,328,329 "
                "po:verbo ts:transitiva al:tráer al:trouxer al:tróuxer\\n"
                "tráer/666,340,342 st:traer\\n"
                "trouxer/666,323,324,325 st:traer\\n"
                "tróuxer/666,341 st:traer\n"
            )
        elif infinitive == "trasler":
            line = "trasler/410,412,414 po:verbo ts:intransitiva\n"
        elif infinitive == "valer":
            line = (
                "valer/320,321,322,324,325,327,328,329,420,421,422,424,425,"
                "427,428,429,520,521,522,524,525,527,528,529 "
                "po:verbo ts:transitiva ts:intransitiva ts:pronominal "
                "al:váler al:valler al:váller\\n"
                "váler/666,340,341,440,441,540,541 st:valer\\n"
                "valler/666,326,426,526 st:valer\\n"
                "váller/666,342,442,542 st:valer\n"
            )
        elif infinitive == "tremelucir":
            line = (
                "tremelucir/750,751 po:verbo ts:defectiva_intransitiva "
                "al:tremelocir al:tremelócir al:tremelúcir\\n"
                "tremelocir/666,753 st:tremelucir\\n"
                "tremelócir/666,753 st:tremelucir\\n"
                "tremelúcir/666,752 st:tremelucir\n"
            )
        elif infinitive == "ver":
            line = (
                "ver/320,321,322,327,328,329,420,421,422,427,428,429,520,521,"
                "522,527,528,529 "
                "po:verbo ts:transitiva ts:intransitiva ts:pronominal "
                "al:vexer al:véxer al:vir\\n"
                "vexer/666,326,426,526 st:ver\\n"
                "véxer/666,342,442,542 st:ver\\n"
                "vir/666,323,324,325,423,424,425,523,524,525 st:ver\n"
            )
        elif infinitive == "vir":
            line = (
                "vir/640,641,740,741,840,841 "
                "po:verbo ts:intransitiva ts:pronominal ts:auxiliar al:viñer\\n"
                "viñer/666,641,642,643,644,740,741,742,743,744,841,842,842,"
                "843,844 st:vir\n"
            )
        else:
            allomorphs = gl_sorted(get_allomorphs(infinitive, conjugation))
            i_p_p1 = conjugation.get("i_p_p1")
            i_p_p4 = conjugation.get("i_p_p4")
            i_p_p6 = conjugation.get("i_p_p6")
            s_p_p4 = conjugation.get("s_p_p4")
            flag_modes = ALL_MODES if auxiliar else modes
            flags = get_verb_flags(infinitive, flag_modes, conjugation)
            flag_string = "/" + ",".join(str(flag) for flag in sorted(flags))
            ts = " ".join(f"ts:{mode}" for mode in ALL_MODES if mode in modes)
            if isinstance(i_p_p1, dict):
                for po, _i_p_p1 in i_p_p1.items():
                    if _i_p_p1 != "-":
                        continue
                    mode = po.split(" ")[-1]
                    if i_p_p4.get(po) == "-" or s_p_p4.get(po) == "-":
                        if i_p_p6.get(po) == "-" and i_p_p4.get(po) == "-":
                            ts = ts.replace(f"ts:{mode}", f"ts:impersoal_{mode}")
                        else:
                            ts = ts.replace(f"ts:{mode}", f"ts:defectiva_{mode}")
            elif i_p_p4 == "-" or s_p_p4 == "-":
                if i_p_p6 == "-" and i_p_p4 == "-":
                    ts = ts.replace("ts:", "ts:impersoal_")
                else:
                    assert i_p_p1 == "-"
                    ts = ts.replace("ts:", "ts:defectiva_")
            ts = ts.replace("ivo", "iva")
            if auxiliar:
                ts += " ts:auxiliar"
            line = f"{infinitive}{flag_string} po:verbo {ts}"
            if allomorphs:
                al = " ".join(f"al:{allomorph}" for allomorph in allomorphs)
                line += f" {al}"
            line += "\\n"
            for allomorph in allomorphs:
                flags = get_allomorph_flags(
                    infinitive, allomorph, flag_modes, conjugation
                )
                flag_string = "/666," + ",".join(str(flag) for flag in sorted(flags))
                line += f"{allomorph}{flag_string} st:{infinitive}\\n"
            line = line[:-2] + "\n"
        self.write_line(line)

    def add(self, entry):
        if DEBUG_WORD == entry.lemma:
            logger.debug(entry.__dict__)
        if entry.status != Status.RECOMMENDED:
            return

        if entry.lemma in {"cisxénero", "cortalumes", "transxénero"}:
            self._add(entry.lemma, po=entry.parts_of_speech)
            return
        if entry.lemma == "el" and entry.parts_of_speech == {"artigo"}:
            self._add("El", po="artigo singular")
            return
        if entry.lemma == "un":
            self._add(
                "un", (10, 17), {"adxectivo", "artigo indeterminado", "indefinido"}
            )
            self._add("un", po="numeral masculino")
            self._add("unha", po="numeral feminino")
            self._add("un", 10, "substantivo masculino")
            return
        if entry.lemma == "meu":
            self._add("meu", 10, "posesivo masculino")
            self._add("miña", 10, "posesivo feminino")
            return
        if entry.lemma == "lle":
            self._add("lle", 10, "pronome persoal")
            return
        if entry.lemma == "no" and entry.parts_of_speech == {"pronome persoal"}:
            self._add("no", (10, 15), "pronome persoal")
            return
        if entry.lemma == "ás" and entry.parts_of_speech == {
            "contracción",
            "substantivo masculino",
        }:
            self._add("ás", (10), "substantivo masculino")
            return
        if re.match(
            rf"^(?:{CONSONANTE}|ch|gu|nh|qu|rr)$", entry.lemma
        ) and entry.parts_of_speech >= {
            "substantivo masculino",
        }:
            self._add(entry.lemma, (), "substantivo masculino")
            entry.parts_of_speech.remove("substantivo masculino")

        self._pos_being_added = set()

        def handle(pos):
            if all(po in entry.parts_of_speech for po in pos):
                other_pos = set(entry.parts_of_speech) - pos
                if entry.invariable is True or entry.lemma in INVARIABLE_SET:
                    pos |= other_pos
                    self._add(entry.lemma, po=pos)
                    entry.parts_of_speech -= pos
                elif all(
                    po in INFLEXIBLE_POS
                    or (
                        entry.invariable if isinstance(entry.invariable, dict) else {}
                    ).get(po)
                    or po == "numeral"
                    and INFLEX_NUM_RE.search(entry.lemma)
                    for po in pos
                ):
                    pos |= other_pos & set(
                        entry.invariable if isinstance(entry.invariable, dict) else {}
                    )
                    pos |= other_pos & INFLEXIBLE_POS
                    if entry.feminine_lemmas and "numeral" in pos:
                        feminine = next(iter(entry.feminine_lemmas))
                        if (
                            entry.lemma.endswith("ntos")
                            and feminine == f"{entry.lemma[:-4]}ntas"
                        ):
                            self._add(entry.lemma, 17, po=pos)
                        else:
                            self._add(entry.lemma, po={f"{po} masculino" for po in pos})
                            for lemma in entry.feminine_lemmas:
                                self._add(lemma, po={f"{po} feminino" for po in pos})
                    elif (
                        pos != set(entry.parts_of_speech)
                        and all("feminino" in po for po in pos)
                        and all("plural" in po for po in pos)
                    ):
                        singular = (
                            next(iter(entry.feminine_lemmas))
                            if entry.feminine_lemmas
                            else entry.lemma
                        )
                        self._add(pluralize(singular), po=pos)
                    elif (
                        pos != set(entry.parts_of_speech)
                        and all("masculino" in po for po in pos)
                        and all("plural" in po for po in pos)
                        and any(
                            po[: -len(" masculino plural")] in other_pos for po in pos
                        )
                    ):
                        pass
                    else:
                        self._add_entry(entry, pos)
                    entry.parts_of_speech -= pos
                elif SPECIAL_PLURALS.get(entry.lemma) == 11:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 11, po=pos)
                        entry.parts_of_speech -= pos
                elif len(entry.feminine_lemmas) == 1 and (
                    (
                        entry.lemma.endswith("que")
                        and next(iter(entry.feminine_lemmas)).endswith("ca")
                    )
                    or (
                        entry.lemma.endswith("eu")
                        and next(iter(entry.feminine_lemmas)).endswith("ía")
                    )
                ):
                    self._add(entry.lemma, (10, 17), pos)
                    entry.parts_of_speech -= pos
                elif (
                    len(entry.feminine_lemmas) == 2
                    and entry.lemma.endswith("ón")
                    and entry.feminine_lemmas
                    == {
                        f"{entry.lemma[:-2]}oa",
                        f"{entry.lemma[:-2]}a",
                    }
                ):
                    self._add(entry.lemma, (10, 15, 17), pos)
                    entry.parts_of_speech -= pos
                elif all(po in FEM_POS for po in pos) and not entry.feminine_lemmas:
                    pos |= {po for po in other_pos if po not in NO_NUMBER_POS}
                    self._add_entry(entry, pos)
                elif any(po in FEM_POS for po in pos) and entry.feminine_lemmas:
                    pos |= {po for po in other_pos if po in FEM_POS}
                    self._add_entry(entry, pos)
                elif all(po in MASC_POS for po in pos):
                    pos |= {po for po in other_pos if po in MASC_POS}
                    self._add_entry(entry, pos)
                elif all("verbo" in po for po in pos):
                    pos |= {po for po in other_pos if "verbo" in po}
                    self._add_verb(
                        entry.lemma,
                        {po[len("verbo ") :] for po in pos},
                        entry.conjugation,
                        entry.auxiliar,
                    )
                    entry.parts_of_speech -= pos
                elif ADJ_10_RE.search(entry.lemma) or entry.singular:
                    self._add_entry(entry, pos)
                elif entry.lemma.endswith("aquel"):
                    if entry.feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            self._add(entry.lemma, (10, 14), pos)
                            entry.parts_of_speech -= pos
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, pos)
                            entry.parts_of_speech -= pos
                elif re.search(r"^[dn]?(?:isto|iso|aquilo)$", entry.lemma):
                    self._add(entry.lemma, po=pos)
                    entry.parts_of_speech -= pos
                elif (
                    len(entry.feminine_lemmas) == 1
                    and next(iter(entry.feminine_lemmas)) == f"{entry.lemma}a"
                ):
                    if entry.lemma.endswith("l"):
                        self._add(entry.lemma, (12, 14), pos)
                    else:
                        self._add(entry.lemma, (10, 14), pos)
                    entry.parts_of_speech -= pos
                elif entry.lemma.endswith("án"):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        if len(entry.feminine_lemmas) == 1 and next(
                            iter(entry.feminine_lemmas)
                        ).endswith("ana"):
                            self._add(entry.lemma, (10, 16), pos)
                        else:
                            self._add(entry.lemma, (10, 15), pos)
                        entry.parts_of_speech -= pos
                elif entry.lemma.endswith("ao"):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        if len(entry.feminine_lemmas) == 1 and next(
                            iter(entry.feminine_lemmas)
                        ).endswith("á"):
                            self._add(entry.lemma, (10, 16), pos)
                        else:
                            self._add(entry.lemma, (10, 15), pos)
                        entry.parts_of_speech -= pos
                elif entry.lemma.endswith("és"):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), pos)
                        entry.parts_of_speech -= pos
                elif entry.lemma.endswith("ón"):
                    self._add_entry(entry, pos)
                elif entry.lemma.endswith("or"):
                    if entry.feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            feminine_rules = set()
                            feminine_lemmas = entry.feminine_lemmas
                            if entry.pos_feminine_lemmas:
                                assert len(entry.pos_feminine_lemmas) == 1
                                assert set(entry.pos_feminine_lemmas) == pos
                                po = next(iter(entry.pos_feminine_lemmas))
                                feminine_lemmas = entry.pos_feminine_lemmas[po]
                            for feminine_lemma in feminine_lemmas:
                                if feminine_lemma.endswith("triz"):
                                    feminine_rules.add(15)
                                else:
                                    feminine_rules.add(14)
                            self._add(entry.lemma, (10, *feminine_rules), pos)
                            entry.parts_of_speech -= pos
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, pos)
                            entry.parts_of_speech -= pos
                elif entry.lemma.endswith("ún"):
                    if entry.feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            self._add(entry.lemma, (10, 17), pos)
                            entry.parts_of_speech -= pos
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, pos)
                            entry.parts_of_speech -= pos
                elif re.search(r"(?:[npu]|ci)s$", entry.lemma):
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, po=pos)
                        entry.parts_of_speech -= pos
                elif entry.lemma[-1] in {
                    "b",
                    "d",
                    "é",
                    "g",
                    "h",
                    "i",
                    "k",
                    "m",
                    "n",
                    "p",
                    "s",
                    "t",
                    "u",
                }:
                    if other_pos <= NO_NUMBER_POS:
                        if entry.feminine_lemmas:
                            if " " not in entry.lemma:
                                if entry.lemma.endswith("s"):
                                    # p. ex. ámbalas, ámbolos
                                    self._add(entry.lemma, 17, pos)
                                else:
                                    # p. ex. cun, cunha
                                    self._add_entry(entry, pos)
                            else:
                                # p. ex. desenvolvedor de programas
                                self._add_entry(entry, pos)
                        elif entry.lemma.endswith("s") and (
                            entry.lemma.endswith("coches")
                            or entry.lemma.startswith("garda")
                            or entry.lemma.startswith("lanza")
                            or entry.lemma.startswith("pousa")
                        ):
                            self._add(entry.lemma, po=pos)
                        else:
                            self._add_entry(entry, pos)
                    elif other_pos <= NO_GENDER_POS:
                        self._add(entry.lemma, 17, pos)
                    entry.parts_of_speech -= pos
                elif entry.lemma[-1] in {"l", "o", "ó"}:
                    self._add_entry(entry, pos)
                elif entry.lemma.endswith("ú"):
                    if entry.feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            self._add(entry.lemma, (10, 15), pos)
                            entry.parts_of_speech -= pos
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, pos)
                            entry.parts_of_speech -= pos
                elif entry.lemma.endswith("x"):
                    self._add(entry.lemma, po=pos)
                    entry.parts_of_speech -= pos

        for inflexible_po in INFLEXIBLE_POS:
            if "locución" in inflexible_po or inflexible_po in {
                "prefixo",
                "verbo latino",
            }:
                continue
            handle({inflexible_po})

        po = "adxectivo"
        if po in entry.parts_of_speech:

            def handle_adjective(
                *,
                other_pos,
                feminine_lemmas,
                unique_invariability=None,
            ):
                if DEBUG_WORD == entry.lemma:
                    logger.debug((other_pos, feminine_lemmas, unique_invariability))
                if ADJ_10_RE.search(entry.lemma):
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, po)
                        entry.parts_of_speech.remove(po)
                elif SPECIAL_PLURALS.get(entry.lemma) == 11:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 11, po)
                        entry.parts_of_speech.remove(po)
                elif ADJ_10_17_RE.search(entry.lemma):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 17), po)
                        entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("f"):  # p. ex. naíf
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, po=po)
                        entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("án"):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        if len(feminine_lemmas) == 1 and next(
                            iter(feminine_lemmas)
                        ).endswith("ana"):
                            self._add(entry.lemma, (10, 16), po)
                            entry.parts_of_speech.remove(po)
                        else:
                            self._add(entry.lemma, (10, 15), po)
                            entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("és"):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), po)
                        entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("ón"):
                    if not feminine_lemmas:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, po)  # e.g. marrón
                            entry.parts_of_speech.remove(po)
                    elif other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 16), po)
                        entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("or"):
                    if feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            feminine_rules = set()
                            for feminine_lemma in feminine_lemmas:
                                if feminine_lemma.endswith("triz"):
                                    feminine_rules.add(15)
                                else:
                                    feminine_rules.add(14)
                            self._add(entry.lemma, (10, *feminine_rules), po)
                            entry.parts_of_speech.remove(po)
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, po)
                            entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("ún"):
                    if feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            self._add(entry.lemma, (10, 15), po)
                            entry.parts_of_speech.remove(po)
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, po)
                            entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("l"):
                    if feminine_lemmas:
                        if unique_invariability == "xénero":
                            self._add(entry.lemma, 12, po)
                            entry.parts_of_speech.remove(po)
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 12, po)
                            entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("o"):
                    if feminine_lemmas:
                        _feminine_lemmas = list(feminine_lemmas)
                        if (
                            len(feminine_lemmas) == 1
                            and _feminine_lemmas[0].endswith("a")
                            and _feminine_lemmas[0][:-1] != entry.lemma[:-1]
                        ):
                            if other_pos <= NO_GENDER_OR_NUMBER_POS:
                                self._add(entry.lemma, (10,), po)
                                self._add(_feminine_lemmas[0], (10,), po)
                                entry.parts_of_speech.remove(po)
                        elif other_pos <= NO_GENDER_OR_NUMBER_POS:
                            self._add(entry.lemma, (10, 15), po)
                            entry.parts_of_speech.remove(po)
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, po)
                            entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("s"):
                    if not other_pos:
                        # p. ex. prínceps
                        self._add(entry.lemma, po=po)
                        entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("t"):
                    if not other_pos:
                        # p. ex. fahrenheit, fondant
                        self._add(entry.lemma, po=po)
                        entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("ú"):
                    if not other_pos:
                        # p. ex. recrú
                        self._add(entry.lemma, (10, 14), po)
                        entry.parts_of_speech.remove(po)
                elif entry.lemma.endswith("x"):
                    if not other_pos:
                        # p. ex. unisex
                        self._add(entry.lemma, po=po)
                        entry.parts_of_speech.remove(po)
                elif DEBUG_WORD == entry.lemma:
                    logger.debug("Exited handle_adjective without changes.")

            if entry.lemma == "asistente":
                # Como substantivo permite cambio de xénero: asistenta.
                self._add(entry.lemma, 10, po)
                entry.parts_of_speech.remove(po)
            elif entry.lemma == "ciano":
                pass  # Non varía, pero tampouco como substantivo masculino
            elif entry.lemma == "gran":
                # Non varía.
                self._add(entry.lemma, po=po)
                entry.parts_of_speech.remove(po)
            elif entry.lemma == "locomotor":
                self._add(entry.lemma, (10, 14, 15), po)
                entry.parts_of_speech.remove(po)
            else:
                other_pos = set(entry.parts_of_speech)
                other_pos.remove(po)
                if entry.invariable is True:
                    if not other_pos:
                        self._add(entry.lemma, po=po)
                        entry.parts_of_speech.remove(po)
                elif entry.invariable is not False and po in entry.invariable:

                    def get_unique_invariability():
                        for value in (True, "número", "xénero"):
                            if entry.invariable[po] == value:
                                if any(
                                    entry.invariable.get(other_po) == value
                                    for other_po in other_pos
                                ):
                                    break
                                return value
                        return None

                    unique_invariability = get_unique_invariability()
                    if unique_invariability is True:
                        self._add(entry.lemma, po=po)
                        entry.parts_of_speech.remove(po)
                    elif unique_invariability is not None:
                        handle_adjective(
                            other_pos=other_pos,
                            unique_invariability=unique_invariability,
                            feminine_lemmas=entry.feminine_lemmas,
                        )
                elif entry.pos_feminine_lemmas:
                    feminine_lemmas_to_pos = defaultdict(set)
                    for pos, feminine_lemmas in list(entry.pos_feminine_lemmas.items()):
                        feminine_lemmas_to_pos[tuple(feminine_lemmas)].add(pos)
                    for feminine_lemmas, pos in list(feminine_lemmas_to_pos.items()):
                        if po in pos:
                            pos.remove(po)
                            handle_adjective(
                                other_pos=pos,
                                feminine_lemmas=feminine_lemmas,
                            )
                            break
                        other_pos -= pos
                    else:
                        handle_adjective(
                            other_pos=other_pos,
                            feminine_lemmas=entry.feminine_lemmas,
                        )
                else:
                    handle_adjective(
                        other_pos=other_pos,
                        feminine_lemmas=entry.feminine_lemmas,
                    )

        handle({"abreviatura", "adverbio"})
        handle({"abreviatura", "preposición"})
        handle({"abreviatura", "símbolo"})
        handle({"adverbio", "conxunción"})
        handle({"adverbio", "conxunción", "preposición"})
        handle({"adverbio", "indefinido", "substantivo masculino"})
        handle({"adverbio", "interxección"})
        handle({"adverbio", "interxección", "preposición"})
        handle({"adverbio", "preposición"})
        handle(
            {"adverbio", "preposición", "substantivo masculino", "substantivo plural"}
        )
        handle({"adverbio", "substantivo masculino"})
        handle({"adxectivo", "artigo indeterminado", "indefinido"})
        handle({"adxectivo", "indefinido"})
        handle({"adxectivo", "numeral"})
        handle({"adxectivo", "numeral", "substantivo masculino"})
        handle({"adxectivo", "participio"})
        handle({"adxectivo", "participio", "substantivo"})
        handle({"adxectivo", "participio", "substantivo masculino"})
        handle({"adxectivo", "substantivo"})
        handle({"adxectivo", "substantivo feminino"})
        handle({"adxectivo", "substantivo masculino"})
        handle({"adxectivo feminino"})
        handle({"adxectivo masculino"})
        handle({"adxectivo masculino", "substantivo masculino"})
        handle({"adxectivo plural"})
        handle({"artigo"})
        handle({"artigo determinado", "pronome persoal"})
        handle({"contracción"})
        handle({"contracción", "substantivo"})
        handle({"demostrativo"})
        handle({"demostrativo", "substantivo"})
        handle({"demostrativo neutro"})
        handle({"exclamativo"})
        handle({"exclamativo", "indefinido", "interrogativo", "relativo"})
        handle({"indefinido"})
        handle({"interrogativo"})
        handle({"interrogativo", "relativo", "substantivo"})
        handle({"interxección", "substantivo feminino plural"})
        handle({"numeral"})
        handle({"numeral", "substantivo"})
        handle({"numeral", "substantivo feminino"})
        handle({"numeral", "substantivo masculino"})
        handle({"numeral ordinal"})
        handle({"participio"})
        handle({"participio", "substantivo"})
        handle({"participio", "substantivo feminino"})
        handle({"participio", "substantivo masculino"})
        handle({"posesivo"})
        handle({"pronome"})
        handle({"pronome", "substantivo"})
        handle({"pronome latino"})
        handle({"pronome persoal"})
        handle({"pronome persoal", "substantivo masculino"})
        handle({"relativo"})
        handle({"relativo", "substantivo masculino"})
        handle({"substantivo"})
        handle({"substantivo feminino"})
        handle({"substantivo masculino"})
        handle({"verbo intransitivo"})
        handle({"verbo pronominal"})
        handle({"verbo transitivo"})

        for known_pos in (
            "abreviatura",
            "adverbio",
            "adverbio_latino",
            "adxectivo",
            "adxectivo feminino",
            "adxectivo masculino",
            "adxectivo plural",
            "artigo",
            "artigo determinado",
            "artigo indeterminado",
            "contracción",
            "conxunción",
            "demostrativo",
            "exclamativo",
            "indefinido",
            "interrogativo",
            "interxección",
            "interxección latina",
            "numeral",
            "numeral ordinal",
            "participio",
            "posesivo",
            "preposición",
            "pronome",
            "pronome latino",
            "pronome persoal",
            "relativo",
            "sigla",
            "símbolo",
            "substantivo",
            "substantivo feminino",
            "substantivo feminino plural",
            "substantivo masculino",
            "substantivo masculino plural",
            "verbo intransitivo",
            "verbo pronominal",
            "verbo transitivo",
        ):
            if (
                known_pos in entry.parts_of_speech
                and known_pos not in self._pos_being_added
                and not re.search(r"^(\w\.)+$", entry.lemma)
            ):
                print(
                    f'Aviso: a función como {known_pos.replace("_", " ")} '
                    f"de «{entry.lemma}» non casou con ningunha regra. "
                    f"Categorías gramaticais: {entry.parts_of_speech!r}"
                )

    def build(self, input_path):
        for data in iter_json_lines(input_path):
            self.add(Entry(**data))
        with self._output_path.open("w") as output:
            self._tmp_output_file.close()
            sh.sort(
                str(self._tmp_output_path),
                unique=True,
                _env={"LC_ALL": "gl_ES.UTF-8"},
                _out=output,
            )
        sh.sed(
            "-i",
            "-e",
            r"s/\\n/\n/g",
            self._output_path,
        )
