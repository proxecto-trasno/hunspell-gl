import re


def normalize_space(text):
    return re.sub(r"\s+", " ", text).strip()
