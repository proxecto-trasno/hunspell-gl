from os import environ

from loguru import logger
from scrapy import Spider
from scrapy.extensions.httpcache import DummyPolicy


def log_debug_request(f):
    def wrapper(spider, response):
        if spider.debug_word:
            spider.log_request_cache(response.request)
        yield from f(spider, response)

    return wrapper


class Policy(DummyPolicy):
    def is_cached_response_fresh(self, cachedresponse, request):
        return not request.meta.get("refresh_cache", False)

    def is_cached_response_valid(self, cachedresponse, response, request):
        return response.status not in self.ignore_http_codes


DEBUG_WORD = environ.get("DEBUG_WORD", None)
DM = "scrapy.downloadermiddlewares"


class BaseSpider(Spider):
    custom_settings = {
        # Be gentle.
        "AUTOTHROTTLE_ENABLED": True,
        "CONCURRENT_REQUESTS": 1,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 1,
        # Disable unused extensions, downloader middlewares and spider
        # middlewares.
        "EXTENSIONS": {
            "scrapy.extensions.telnet.TelnetConsole": None,
            "scrapy.extensions.memusage.MemoryUsage": None,
        },
        "DOWNLOADER_MIDDLEWARES": {
            f"{DM}.httpauth.HttpAuthMiddleware": None,
            f"{DM}.defaultheaders.DefaultHeadersMiddleware": None,
            f"{DM}.redirect.MetaRefreshMiddleware": None,
            f"{DM}.httpproxy.HttpProxyMiddleware": None,
            f"{DM}.stats.DownloaderStats": None,
        },
        "SPIDER_MIDDLEWARES": {
            "scrapy.spidermiddlewares.offsite.OffsiteMiddleware": None,
            "scrapy.spidermiddlewares.depth.DepthMiddleware": None,
        },
        "COOKIES_ENABLED": False,
        # Cache webpages indefinitely.
        "HTTPCACHE_ENABLED": True,
        "HTTPCACHE_EXPIRATION_SECS": 0,
        "HTTPCACHE_IGNORE_HTTP_CODES": [500, 502],
        "HTTPCACHE_POLICY": Policy,
        # Get Unicode in drag.jl
        "FEED_EXPORT_ENCODING": "utf-8",
        # Keep logging to a minimum.
        "LOG_LEVEL": "DEBUG" if DEBUG_WORD else "INFO",
    }
    debug_word = DEBUG_WORD

    def log_request_cache(self, request):
        fp = self.crawler.request_fingerprinter.fingerprint(request)
        fp_hex = fp.hex()
        cache_path = f"cache/scrapy/drag/{fp_hex[:2]}/{fp_hex}"
        logger.info(f"Caché de {request}: {cache_path}")
