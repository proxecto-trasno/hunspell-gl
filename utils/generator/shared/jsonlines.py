import json


def iter_json_lines(filepath):
    with open(filepath) as input_file:
        for line in input_file:
            yield json.loads(line)
