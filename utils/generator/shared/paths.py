from pathlib import Path


_ROOT_DIR = Path(__file__).parent.parent.parent.parent

GENERATOR_ROOT_DIR = Path(__file__).parent.parent
CACHE_DIR = GENERATOR_ROOT_DIR / "cache"
SRC_DIR = _ROOT_DIR / "src"
