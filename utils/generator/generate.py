from sys import argv

from shared.generator import Generator


if __name__ == "__main__":
    Generator(argv[1]).run()
