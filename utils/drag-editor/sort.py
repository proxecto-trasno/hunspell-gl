#!/usr/bin/env python3

import re
from functools import partial
from os import remove
from pathlib import Path
from shutil import move

from click import command
from icu import Collator, Locale
import sh


@command()
def main():
    tmp_filepath = Path("a")
    tmp_filepath2 = Path("b")
    filepath = Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic"
    collator = Collator.createInstance(Locale("gl_ES.UTF-8"))
    gl_sorted = partial(sorted, key=lambda v: collator.getSortKey("  ".join(v)))

    pattern = re.compile(r"\bpo:verbo\b|\bst:")

    def sortable_verb_line(line):
        st_match = re.search(r"\bst:(\S+)", line)
        if st_match:
            root = st_match[1]
            index = "1"
        else:
            root = line.split("/", maxsplit=1)[0]
            index = "0"
            if "al:" in line:
                old_al = re.search(r"al:\S+(?:\s+al:\S+)*", line)[0]
                als = re.split(r"\s+", old_al)
                new_al = " ".join(gl_sorted(als))
                line = line.replace(old_al, new_al)
        old_flags = re.search(r"(?<=/)\d+(?:,\d+)*", line)[0]
        new_flags = ",".join(
            sorted(old_flags.split(","), key=lambda v: v if v != "666" else "0")
        )
        if old_flags != new_flags:
            line = line.replace(old_flags, new_flags)
        return f"{root[-2:]} {root} {index} {line}"

    def unsortable_verb_line(line):
        return line.split(" ", maxsplit=3)[3]

    def compare(line1, line2):
        line1 = sortable_verb_line(line1)
        line2 = sortable_verb_line(line2)
        return collator.compare(line1, line2)

    with tmp_filepath.open("w") as output_file:
        with filepath.open("r") as input_file:
            for line in input_file:
                if not pattern.search(line):
                    continue
                output_file.write(sortable_verb_line(line))

    with tmp_filepath2.open("w") as output:
        sh.sort(
            str(tmp_filepath),
            unique=True,
            _env={"LC_ALL": "gl_ES.UTF-8"},
            _out=output,
        )

    in_match = False
    with tmp_filepath.open("w") as output_file:
        with tmp_filepath2.open("r") as mid_file:
            with filepath.open("r") as input_file:
                for line in input_file:
                    if not pattern.search(line):
                        output_file.write(line)
                    elif in_match:
                        continue
                    else:
                        for _line in mid_file:
                            output_file.write(unsortable_verb_line(_line))
                        in_match = True

    remove(filepath)
    remove(tmp_filepath2)
    move(tmp_filepath, filepath)


if __name__ == "__main__":
    main()
