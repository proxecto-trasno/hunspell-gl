#!/usr/bin/env python3

from pathlib import Path
from os import remove
from shutil import move
from tempfile import TemporaryDirectory

from click import command


@command()
def main():
    build_dir = TemporaryDirectory()
    tmp_filepath = Path(build_dir.name) / "a"
    original_filepath = Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic"

    index = 1
    previous_line = None
    with tmp_filepath.open("w") as output_file:
        with original_filepath.open("r") as input_file:
            for line in input_file:
                if line == previous_line:
                    print(f"-{line.rstrip()} ({index})")
                    index += 1
                    continue
                previous_line = line
                output_file.write(line)

    remove(original_filepath)
    move(tmp_filepath, original_filepath)


if __name__ == "__main__":
    main()
