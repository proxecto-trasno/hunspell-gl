#!/usr/bin/env python3

import re
from pathlib import Path
from os import remove
from shutil import move
from tempfile import TemporaryDirectory

from click import command
from icu import Collator, Locale


@command()
def main():
    build_dir = TemporaryDirectory()
    tmp_filepath = Path(build_dir.name) / "a"
    original_filepath = Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic"
    generated_filepath = (
        Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic~"
    )
    Collator.createInstance(Locale("gl_ES.UTF-8"))

    pattern = r"^(\S+ st:\S+).*$"
    template = "{}\n"
    to_remove = set()
    with original_filepath.open("r") as input_file:
        for line in input_file:
            match = re.search(pattern, line)
            if not match:
                continue
            to_remove_line = template.format(match[1])
            to_remove.add(to_remove_line)

    with generated_filepath.open("r") as input_file:
        for line in input_file:
            if line in to_remove:
                to_remove.remove(line)

    index = 1
    with tmp_filepath.open("w") as output_file:
        with original_filepath.open("r") as input_file:
            for line in input_file:
                if line in to_remove:
                    to_remove.remove(line)
                    print(f"-{line.rstrip()} ({index})")
                    index += 1
                    continue
                output_file.write(line)

    remove(original_filepath)
    move(tmp_filepath, original_filepath)


if __name__ == "__main__":
    main()
