#!/usr/bin/env python3

import re
from pathlib import Path
from os import remove
from shutil import move
from tempfile import TemporaryDirectory

from click import command


@command()
def main():
    build_dir = TemporaryDirectory()
    tmp_filepath = Path(build_dir.name) / "a"
    original_filepath = Path(__file__).parent.parent.parent / "src/rag/gl/correcto.dic"

    previous_line = None
    with tmp_filepath.open("w") as output_file:
        with original_filepath.open("r") as input_file:
            for line in input_file:
                if line == previous_line:
                    continue
                line = re.sub(r"^(\S+ st:\S+)(\s+.+$)$", r"\1", line)
                previous_line = line
                output_file.write(line)

    remove(original_filepath)
    move(tmp_filepath, original_filepath)


if __name__ == "__main__":
    main()
